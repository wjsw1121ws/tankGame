package com.source3g.platform.controller;

import com.source3g.platform.dto.ApiResult;
import com.source3g.platform.dto.GameMap;
import com.source3g.platform.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.source3g.platform.contants.Constant.TEAM_B;
import static com.source3g.platform.contants.Constant.TEAM_C;

/**
 * 控制器
 */
@RestController
@RequestMapping(path = "/player")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @PostMapping(path = "/init")
    public ApiResult init(@RequestBody GameMap gameMap) {
        // TODO 初始化逻辑
        return ApiResult.success("/init", "succeeded");
    }

    @PostMapping(path = "/action")
    public ApiResult action(@RequestBody GameMap gameMap) {
        // TODO 业务逻辑，以下只是单独简单的移动、攻击
        return ApiResult.success("/action", playerService.action(gameMap));
    }

}
