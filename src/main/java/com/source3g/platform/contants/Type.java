package com.source3g.platform.contants;

/**
 * action类型
 */
public enum Type {
    MOVE, // 移动
    FIRE // 开火
}
