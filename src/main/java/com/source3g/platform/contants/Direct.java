package com.source3g.platform.contants;

/**
 * 方向
 */
public enum Direct {
    UP,
    DOWN,
    RIGHT,
    LEFT,
    WAIT
}
