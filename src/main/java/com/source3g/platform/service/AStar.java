package com.source3g.platform.service;

import com.alibaba.fastjson.JSON;
import com.source3g.platform.contants.Direct;
import com.source3g.platform.contants.Type;
import com.source3g.platform.dto.TankAction;
import com.source3g.platform.dto.TankPosition;
import com.source3g.platform.dto.View;
import org.springframework.util.CollectionUtils;

import java.util.*;

//第一遍看懂了怎么找到最短路径，但是没看出它是怎么避过障碍物的，可是它却实现了避过障碍物
public class AStar {
    public static final String[][] NODES = {
            {"M1", "M1", "M1", "M1", "M1", "M1", "M1", "M1"},
            {"M1", "M1", "M1", "M1", "M1", "M1", "M1", "M1"},
            {"M1", "M1", "M1", "M1", "M1", "M1", "M1", "M1"},
            {"M1", "M3", "M3", "M3", "M1", "M1", "M1", "M1"},
            {"M3", "M3", "M1", "M3", "M3", "M3", "M1", "M1"},
            {"M3", "M1", "M1", "M1", "M1", "M1", "M1", "M1"},
            {"M3", "M1", "M1", "M3", "M1", "M1", "M1", "M1"},
            {"M1", "M1", "M1", "M3", "M1", "M1", "M1", "M1"},
            {"M1", "M1", "M1", "M1", "M1", "M1", "M1", "M1"}
    };

    public static final int STEP = 10;

    private ArrayList<Node> openList = new ArrayList<Node>();
    private ArrayList<Node> closeList = new ArrayList<Node>();

    public Node findMinFNodeInOpenList() {
        Node tempNode = openList.get(0); // 先以第一个元素的F为最小值，然后遍历openlist的所有值，找出最小值
        for (Node node : openList) {
            if (node.F < tempNode.F) {
                tempNode = node;
            }
        }
        return tempNode;
    }

    // 考虑周围节点的时候，就不把节点值为1的节点考虑在内，所以自然就直接避开了障碍物
    public ArrayList<Node> findNeighborNodes(List<List<String>> nodes, Node currentNode, int row, int col, List<String> type) {
        ArrayList<Node> arrayList = new ArrayList<Node>();
        // 只考虑上下左右，不考虑斜对角
        int topX = currentNode.x;
        int topY = currentNode.y - 1;
        // canReach方法确保下标没有越界 exists方法确保此相邻节点不存在于closeList中，也就是之前没有遍历过
        if (canReach(nodes, topX, topY, row, col, type) && !exists(closeList, topX, topY)) {
            arrayList.add(new Node(topX, topY));
        }
        int bottomX = currentNode.x;
        int bottomY = currentNode.y + 1;
        if (canReach(nodes, bottomX, bottomY, row, col, type) && !exists(closeList, bottomX, bottomY)) {
            arrayList.add(new Node(bottomX, bottomY));
        }
        int leftX = currentNode.x - 1;
        int leftY = currentNode.y;
        if (canReach(nodes, leftX, leftY, row, col, type) && !exists(closeList, leftX, leftY)) {
            arrayList.add(new Node(leftX, leftY));
        }
        int rightX = currentNode.x + 1;
        int rightY = currentNode.y;
        if (canReach(nodes, rightX, rightY, row, col, type) && !exists(closeList, rightX, rightY)) {
            arrayList.add(new Node(rightX, rightY));
        }
        return arrayList;
    }

    public boolean canReach(List<List<String>> nodes, int x, int y, int row, int col, List<String> type) {
        if (x >= 0 && x < row && y >= 0 && y < col) {
            // 原来是在这里避过障碍物的啊。。如果节点值不为M1，说明不可到达。
            return type.contains(nodes.get(x).get(y));
        }
        return false;
    }

    public Node findPath(List<List<String>> nodes, Node startNode, Node endNode, int row, int col, List<String> type) {

        // 把起点加入 open list
        openList.add(startNode);

        while (openList.size() > 0) {
            // 遍历 open list ，查找 F值最小的节点，把它作为当前要处理的节点
            Node currentNode = findMinFNodeInOpenList();

            // F值最小的节点从open list中移除
            openList.remove(currentNode);
            // 把这个节点移到 close list，closelist就是存储路径的链表
            closeList.add(currentNode);

            // 查找不存在于close list当中的周围节点（不考虑斜边的邻居）
            ArrayList<Node> neighborNodes = findNeighborNodes(nodes, currentNode, row, col, type);

            // openlist其实就是存储的外围的节点集合
            for (Node node : neighborNodes) {// 总之要把邻居节点添加进openlist当中
                if (exists(openList, node)) { // 如果邻居节点在openlist当中
                    foundPoint(currentNode, node);
                } else {
                    // 如果邻居节点不在openlist中，那就添加进openlist
                    notFoundPoint(currentNode, endNode, node);
                }
            }
            // 如果在openlist中找到了终点，那么就说明已经找到了路径，返回终点
            if (find(openList, endNode) != null) {
                return find(openList, endNode);
            }
        }

        return find(openList, endNode);
    }

    // 此种情况就是发现周围的F值最小的节点是之前已经遍历过了的，所以这个节点的G,H,F值都是已经计算过了的
    // 此时H值肯定不会变，所以要比较G值，如果现在的G值比之前的小，说明现在的路径更优
    // 接着就重置此节点的父指针，G值和F值
    private void foundPoint(Node tempStart, Node node) {
        int G = calcG(tempStart, node);
        if (G < node.G) {
            node.parent = tempStart;
            node.G = G;
            node.calcF();
        }
    }

    // 这种情况是之前没有计算过此节点的值，所以在这里要计算一遍G,H,F值，然后确认父指针指向，然后加入openlist当中
    private void notFoundPoint(Node tempStart, Node end, Node node) {
        node.parent = tempStart;
        node.G = calcG(tempStart, node);
        node.H = calcH(end, node);
        node.calcF();
        openList.add(node);
    }

    private int calcG(Node start, Node node) {
        int G = STEP;
        int parentG = node.parent != null ? node.parent.G : 0;
        return G + parentG;
    }

    // 这个是最简单粗暴的计算H值得方法，当然还有其它方法，这里先理解AStar的思想先，以后可以自己改进这个计算H值得方法
    private int calcH(Node end, Node node) {
        int step = Math.abs(node.x - end.x) + Math.abs(node.y - end.y);
        return step * STEP;
    }

    public List<Node> getRoute(View view, List<Integer> position, List<Integer> destination, List<String> type){
        List<List<String>> map = view.getMap();
        int rowLen = view.getRowLen();
        int colLen = view.getColLen();
        Node start = new Node(position.get(0), position.get(1));
        Node end = new Node(destination.get(0), destination.get(1));
        // 返回的是终点，但是此时父节点已经确立，可以追踪到开始节点
        Node parent = new AStar().findPath(map, start, end, rowLen, colLen, type);
        List<Node> list = new ArrayList<Node>();
        // 遍历刚才找到的路径。没问题
        while (parent != null) {
            list.add(new Node(parent.x, parent.y));
            parent = parent.parent;
        }
        return list;
    }


    public List<List<Integer>> getNextLocation(View view, List<Integer> position, List<Integer> destination, List<String> type) {
        List<Node> list = getRoute(view, position, destination, type);
        System.out.println("position" + position);
        System.out.println("destination" + destination);
        System.out.println();
        for (Node node : list) {
            System.out.print(node.getX() + ":" + node.getY() + ",");
        }
        System.out.println();
        List<List<Integer>> result = new LinkedList<>();
        if (list.size() == 2) {
            result.add(Arrays.asList(list.get(0).getX(), list.get(0).getY()));
        }
        if (list.size() > 2) {
            result.add(Arrays.asList(list.get(list.size() - 2).getX(), list.get(list.size() - 2).getY()));
            result.add(Arrays.asList(list.get(list.size() - 3).getX(), list.get(list.size() - 3).getY()));
        }
        System.out.println("result" + result);
        return result;
    }

    public void move(TankAction tankAction, View view, TankPosition tankPosition, List<Integer> destination, List<String> type) {
        List<Integer> position = tankPosition.getXy();
        int yiDong = tankPosition.getTank().getYidong();
        Integer positionX = position.get(0);
        Integer positionY = position.get(1);
        tankAction.setType(Type.MOVE);
        List<List<Integer>> nextLocation = getNextLocation(view, position, destination, type);
        System.out.println("nextLocation" + nextLocation);
        if (CollectionUtils.isEmpty(nextLocation)) {
            new PlayerCommon().blockMove(tankAction, tankPosition);
        }

        if (nextLocation.size() == 2) {
            List<Integer> list1 = nextLocation.get(0);
            List<Integer> list2 = nextLocation.get(1);
            Integer x1 = list1.get(0);
            Integer y1 = list1.get(1);
            Integer x2 = list2.get(0);
            Integer y2 = list2.get(1);
            if (positionX.equals(x1) && positionX.equals(x2) && positionY > y1) {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.LEFT);
            }
            if (positionX.equals(x1) && positionX.equals(x2) && positionY < y1) {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.RIGHT);
            }
            if (positionY.equals(y1) && positionY.equals(y2) && positionX > x1) {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.UP);
            }
            if (positionY.equals(y1) && positionY.equals(y2) && positionX < x1) {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.DOWN);
            } else {
                if (positionX.equals(x1) && positionY > y1) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.LEFT);
                }
                if (positionX.equals(x1) && positionY < y1) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.RIGHT);
                }
                if (positionY.equals(y1) && positionX > x1) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.UP);
                }
                if (positionY.equals(y1) && positionX < x1) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.DOWN);
                }
            }
        }

        if (nextLocation.size() == 1) {
            List<Integer> list = nextLocation.get(0);
            Integer x = list.get(0);
            Integer y = list.get(1);
            if (positionX.equals(x) && positionY > y) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.LEFT);
            }
            if (positionX.equals(x) && positionY < y) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.RIGHT);
            }
            if (positionY.equals(y) && positionX > x) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.UP);
            }
            if (positionY.equals(y) && positionX < x) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.DOWN);
            }
        }
        System.out.println("tankAction" + JSON.toJSONString(tankAction));
    }

    public static Node find(List<Node> nodes, Node point) {
        for (Node n : nodes) {
            if ((n.x == point.x) && (n.y == point.y)) {
                return n;
            }
        }
        return null;
    }

    public static boolean exists(List<Node> nodes, Node node) {
        for (Node n : nodes) {
            if ((n.x == node.x) && (n.y == node.y)) {
                return true;
            }
        }
        return false;
    }

    public static boolean exists(List<Node> nodes, int x, int y) {
        for (Node n : nodes) {
            if ((n.x == x) && (n.y == y)) {
                return true;
            }
        }
        return false;
    }

    public static class Node {
        public Node(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int x;
        public int y;

        public int F;
        public int G;
        public int H;

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getF() {
            return F;
        }

        public void setF(int f) {
            F = f;
        }

        public int getG() {
            return G;
        }

        public void setG(int g) {
            G = g;
        }

        public int getH() {
            return H;
        }

        public void setH(int h) {
            H = h;
        }

        public Node getParent() {
            return parent;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }

        public void calcF() {
            this.F = this.G + this.H;
        }

        public Node parent;
    }

}