package com.source3g.platform.service;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.source3g.platform.contants.Direct;
import com.source3g.platform.contants.Type;
import com.source3g.platform.dto.*;
import com.sun.org.apache.bcel.internal.generic.RET;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @description:
 * @author: W10005176
 * @version: 1.0
 * @blame: Test Team
 **/
public class PlayerCommon {
    Logger logger = LoggerFactory.getLogger(PlayerCommon.class);
    /*
    M1	            路
	M2	            复活币
	M3	            迷雾
	M4	            障碍物1
	M5	            障碍物2
	M6	            障碍物3
	M7	            障碍物4
	M8	            障碍物5
	A1	            BOSS
	B1	            B队K2黑豹（攻击强）
	B2	            B队T-90（防守强）
	B3	            B队阿马塔（移动快）
	B4	            B队阿马塔（移动快）
	B5	            B队99主战坦克（射程远）
	C1	            C队K2黑豹（攻击强）
	C2	            C队T-90（防守强）
	C3	            C队阿马塔（移动快）
	C4	            C队阿马塔（移动快）
	C5	            C队99主战坦克（射程远）

    5种坦克属性，包括：攻击、防御、移动、视野、射程
	坦克属性值表
	坦克名称           攻击   防御   移动   射程   视野
	K2黑豹(攻击强)     3      5        1      1      1
	T-90(防守强)      1      10       1      1      1
	阿玛塔(移动快)     1      4        2      1      2
	99主战坦克        1      3        1      3      1
    * */
    public static List<TankAction> tankPoZhang;
    public static List<TankAction> tankMove2;
    public static List<TankAction> tankMove;
    List<String> luZhang = Arrays.asList("M4", "M5", "M6", "M7", "M8");
    List<String> teamB = Arrays.asList("B1", "B2", "B3", "B4", "B5");
    List<String> teamC = Arrays.asList("C1", "C2", "C3", "C4", "C5");


    public TankAction operate(TankAction tankAction, Tank tank, View view) {
        Map<String, List<List<Integer>>> zhangAiMap = getLocationsByTankIds(view.getMap(), "M4", "M5", "M6", "M7", "M8");
        Map<String, List<List<Integer>>> tankMapB = getLocationsByTankIds(view.getMap(), "B1", "B2", "B3", "B4", "B5");
        Map<String, List<List<Integer>>> tankMapC = getLocationsByTankIds(view.getMap(), "C1", "C2", "C3", "C4", "C5");
        Map<String, List<List<Integer>>> boss = getLocationsByTankIds(view.getMap(), "A1");
        Map<String, List<List<Integer>>> m2 = getLocationsByTankIds(view.getMap(), "M2");
        Map<String, List<List<Integer>>> m3 = getLocationsByTankIds(view.getMap(), "M3");
        if (tank.getShengyushengming() == 0) {
            // 已经牺牲了，如果要移动就需要消耗复活币
            tankAction.setUseGlod(true);
        }
        String tankId = tank.gettId();
        // 获取当前坦克所在坐标
        Map<String, List<List<Integer>>> tankMap = getLocationsByTankIds(view.getMap(), tankId);
        // 判断坦克是否存在-可能是已死亡
        if (CollectionUtils.isEmpty(tankMap)) {
            return tankAction;
        }
        // 所有的障碍物、boss和坦克作为障碍物避障（注意要排除自己）
        Map<String, List<List<Integer>>> zhangAi = new HashMap<>();
        zhangAi.putAll(zhangAiMap);
        zhangAi.putAll(tankMapB);
        zhangAi.putAll(tankMapC);
        zhangAi.putAll(boss);
        zhangAi.remove(tankId);
        List<List<Integer>> zhangAiList = new ArrayList<>();
        for (Map.Entry<String, List<List<Integer>>> entry : zhangAi.entrySet()) {
            zhangAiList.addAll(entry.getValue());
        }

        List<List<Integer>> tankBList = tankMapB.entrySet().stream().flatMap(entry -> entry.getValue().stream()).collect(Collectors.toList());
        List<List<Integer>> tankCList = tankMapC.entrySet().stream().flatMap(entry -> entry.getValue().stream()).collect(Collectors.toList());
        List<List<Integer>> diFangTankList = new ArrayList<>();
        Map<String, List<List<Integer>>> diFangTankMap;
        if (tankId.startsWith("B")) {
            if (!CollectionUtils.isEmpty(tankCList)) {
                diFangTankList = tankCList;
                diFangTankMap = tankMapC;

            }
        } else {
            if (!CollectionUtils.isEmpty(tankBList)) {
                diFangTankList = tankBList;
                diFangTankMap = tankMapB;
            }
        }
        TankPosition tankPosition = new TankPosition(view, tank);
        // 获取复活币--优先级0
        // 表示地图上存在复活币
        List<List<Integer>> m2Position = m2.get("M2");
        if (!CollectionUtils.isEmpty(m2) && !CollectionUtils.isEmpty(m2Position)) {
            // 获取当前距离最近的复活币
            boolean flag = collectCoin(tankAction, view, tankPosition, m2.get("M2"));
            if (flag) {
                return tankAction;
            }
            if (!CollectionUtils.isEmpty(boss) && !CollectionUtils.isEmpty(boss.get("A1"))) {
                attractBoss(tankAction, view, tankPosition, boss, zhangAiList);
            }
            // 攻击敌方坦克-优先级2
            else if (!CollectionUtils.isEmpty(diFangTankList)) {
                attractDiFang(tankAction, view, tankPosition, zhangAiList);
            }
            // 迷雾-优先级3
            else if (!CollectionUtils.isEmpty(m3) && !CollectionUtils.isEmpty(m3.get("M3"))) {
                blockMove(tankAction, new TankPosition(view, tank));
            }

        }
        // 存在BOSS-优先级1
        else if (!CollectionUtils.isEmpty(boss) && !CollectionUtils.isEmpty(boss.get("A1"))) {
            attractBoss(tankAction, view, tankPosition, boss, zhangAiList);
        }

        // 攻击敌方坦克-优先级2
        else if (!CollectionUtils.isEmpty(diFangTankList)) {
            attractDiFang(tankAction, view, tankPosition, zhangAiList);
        }
        // 迷雾-优先级3
        else if (!CollectionUtils.isEmpty(m3) && !CollectionUtils.isEmpty(m3.get("M3"))) {
            blockMove(tankAction, new TankPosition(view, tank));
        }
        return tankAction;
    }

    public void attractBoss(TankAction tankAction, View view, TankPosition tankPosition, Map<String, List<List<Integer>>> boss, List<List<Integer>> zhangAiList) {
        List<String> list = Stream.of(tankPosition.getDown(), tankPosition.getUp(), tankPosition.getLeft(), tankPosition.getRight()).distinct().filter(str -> !StringUtils.isEmpty(str)).collect(Collectors.toList());
        List<String> aroundTankDet;
        if (tankPosition.getIsTeamB()) {
            aroundTankDet = list.stream().filter(x -> x.startsWith("C")).collect(Collectors.toList());
        } else {
            aroundTankDet = list.stream().filter(x -> x.startsWith("B")).collect(Collectors.toList());
        }
        if (list.contains("A1") && !CollectionUtils.isEmpty(aroundTankDet)) {
            String diFangTankId = attractSequence(tankPosition.getTank(), aroundTankDet);
            Map<String, List<List<Integer>>> diFangTankIdList = getLocationsByTankId(view.getMap(), diFangTankId);
            fire(tankAction, 1, tankPosition.getXy(), diFangTankIdList.get(diFangTankId).get(0));
        } else if (list.contains("A1")) {
            fire(tankAction, 1, tankPosition.getXy(), boss.get("A1").get(0));
        } else {
            List<List<Integer>> aroundCorn = getAvailableLocation(1, view, boss.get("A1").get(0), zhangAiList);
            // 表示BOSS附近存在可占位置
            if (!CollectionUtils.isEmpty(aroundCorn)) {
                List<Integer> leastPoint = getLeastPoint(tankPosition.getXy(), aroundCorn);
                new AStar().move(tankAction, view, tankPosition, leastPoint, Collections.singletonList("M1"));
            } else {
                attractDiFang(tankAction, view, tankPosition, zhangAiList);
            }
        }
    }

    public void attractDiFang(TankAction tankAction, View view, TankPosition tankPosition, List<List<Integer>> zhangAiList) {
        List<String> list = Stream.of(tankPosition.getDown(), tankPosition.getUp(), tankPosition.getLeft(), tankPosition.getRight()).distinct().filter(str -> !StringUtils.isEmpty(str)).collect(Collectors.toList());
        Map<String, List<List<Integer>>> tankMapB = getLocationsByTankIds(view.getMap(), "B1", "B2", "B3", "B4", "B5");
        Map<String, List<List<Integer>>> tankMapC = getLocationsByTankIds(view.getMap(), "C1", "C2", "C3", "C4", "C5");
        List<List<Integer>> tankBList = tankMapB.entrySet().stream().flatMap(entry -> entry.getValue().stream()).collect(Collectors.toList());
        List<List<Integer>> tankCList = tankMapC.entrySet().stream().flatMap(entry -> entry.getValue().stream()).collect(Collectors.toList());
        List<List<Integer>> diFangTankList = new ArrayList<>();
        List<List<Integer>> myTankMap = new ArrayList<>();
        String tankId = tankPosition.getTank().gettId();
        Map<String, List<List<Integer>>> diFangTankMap = new HashMap<>();
        String tankPrefix = "";
        if (tankId.startsWith("B")) {
            if (!CollectionUtils.isEmpty(tankCList)) {
                diFangTankList = tankCList;

            }
        } else {
            if (!CollectionUtils.isEmpty(tankBList)) {
                diFangTankList = tankBList;

            }
        }
        List<String> aroundTankDet;
        if (tankPosition.getIsTeamB()) {
            aroundTankDet = list.stream().filter(x -> x.startsWith("C")).collect(Collectors.toList());
        } else {
            aroundTankDet = list.stream().filter(x -> x.startsWith("B")).collect(Collectors.toList());
        }
        List<List<Integer>> tankAround = new ArrayList<>();
        for (List<Integer> diFangTank : diFangTankList) {
            List<List<Integer>> availableLocation = getAvailableLocation(1, view, diFangTank, zhangAiList);
            tankAround.addAll(availableLocation);
        }
        List<Integer> latestAttract = getLeastPoint(tankPosition.getXy(), tankAround);
        if (!CollectionUtils.isEmpty(list)) {
            if (!CollectionUtils.isEmpty(aroundTankDet)) {
                String diFangTankId = attractSequence(tankPosition.getTank(), aroundTankDet);
                Map<String, List<List<Integer>>> diFangTankLocation = getLocationsByTankId(view.getMap(), diFangTankId);
                fire(tankAction, 1, tankPosition.getXy(), diFangTankLocation.get(diFangTankId).get(0));
            } else {
                new AStar().move(tankAction, view, tankPosition, latestAttract, Collections.singletonList("M1"));
            }
        } else {
            new AStar().move(tankAction, view, tankPosition, latestAttract, Collections.singletonList("M1"));
        }
    }

    public String attractSequence(Tank tank, List<String> diFangList) {
        if (CollectionUtils.isEmpty(diFangList)) {
            return null;
        }

        if (diFangList.size() == 1) {
            return diFangList.get(0);
        }
        String df;
        if (tank.gettId().startsWith("B")) {
            df = "C";
        } else {
            df = "B";
        }
        if (diFangList.contains("df" + "1")) {
            return df + "1";
        }
        if (diFangList.contains("df" + "5")) {
            return df + "5";
        }
        if (diFangList.contains("df" + "2")) {
            diFangList.remove("df" + "2");
            return diFangList.get(0);
        }
        return diFangList.get(0);
    }

    public boolean collectCoin(TankAction tankAction, View view, TankPosition tankPosition, List<List<Integer>> m2) {
        String tankId = tankPosition.getTank().gettId();
        String type1;
        String type2;
        if (tankPosition.getIsTeamB()) {
            type1 = "B3";
            type2 = "B4";
        } else {
            type1 = "C3";
            type2 = "C4";
        }
        Map<String, List<List<Integer>>> t1 = getLocationsByTankId(view.getMap(), type1);
        Map<String, List<List<Integer>>> t2 = getLocationsByTankId(view.getMap(), type2);
        List<AStar.Node> route1 = new ArrayList<>();
        List<AStar.Node> route2 = new ArrayList<>();
        if (!CollectionUtils.isEmpty(t1) && !CollectionUtils.isEmpty(t1.get(type1))) {
            route1 = new AStar().getRoute(view, t1.get(type1).get(0), m2.get(0), Arrays.asList("M1", "M2"));
        }
        if (!CollectionUtils.isEmpty(t2) && !CollectionUtils.isEmpty(t2.get(type2))) {
            route2 = new AStar().getRoute(view, t2.get(type2).get(0), m2.get(0), Arrays.asList("M1", "M2"));
        }
        if (!CollectionUtils.isEmpty(route1) && !CollectionUtils.isEmpty(route2)) {
            if (route1.size() <= route2.size() && type1.equals(tankId)) {
                new AStar().move(tankAction, view, tankPosition, m2.get(0), Arrays.asList("M1", "M2"));
                return true;
            }
            if (route1.size() > route2.size() && type2.equals(tankId)) {
                new AStar().move(tankAction, view, tankPosition, m2.get(0), Arrays.asList("M1", "M2"));
                return true;
            }
        }
        return false;
    }


    /**
     * 获取元素在map中的坐标
     *
     * @param map       地图
     * @param statement 地图中的元素
     */
    public Map<String, List<List<Integer>>> getLocationsByTankIds(List<List<String>> map, String... statement) {
        Map<String, List<List<Integer>>> result = new HashMap<>();
        for (String type : statement) {
            List<List<Integer>> data = new ArrayList<>();
            for (int i = 0; i < map.size(); i++) {
                List<String> list = map.get(i);
                for (int j = 0; j < list.size(); j++) {
                    String x = list.get(j);
                    if (x.equals(type)) {
                        List<Integer> li = new ArrayList<>();
                        li.add(i);
                        li.add(j);
                        data.add(li);
                    }
                }
            }
            if (!CollectionUtils.isEmpty(data)) {
                result.put(type, data);
            }
        }

        return result;
    }

    public static Map<String, List<List<Integer>>> getLocationsByTankId(List<List<String>> map, String statement) {
        Map<String, List<List<Integer>>> result = new HashMap<>();
        List<List<Integer>> data = new ArrayList<>();
        for (int i = 0; i < map.size(); i++) {
            List<String> list = map.get(i);
            for (int j = 0; j < list.size(); j++) {
                String x = list.get(j);
                if (x.equals(statement)) {
                    List<Integer> li = new ArrayList<>();
                    li.add(i);
                    li.add(j);
                    data.add(li);
                }
            }
        }
        if (!CollectionUtils.isEmpty(data)) {
            result.put(statement, data);
        }

        return result;
    }

    public static String getTypeByLocation(List<List<String>> map, List<Integer> location) {
        for (int i = 0; i < map.size(); i++) {
            if (i == location.get(0)) {
                List<String> list = map.get(i);
                for (int j = 0; j < list.size(); j++) {
                    if (j == location.get(1)) {
                        return list.get(j);
                    }
                }
            }
        }
        return null;
    }

    /**
     * 获取距离某个点最近距离的点
     *
     * @param point       某个点
     * @param destination 目的地
     * @return List<Integer>点
     */
    public List<Integer> getLeastPoint(List<Integer> point, List<List<Integer>> destination) {
        Map<Integer, List<Integer>> distanceList = new HashMap<>();
        for (List<Integer> list : destination) {
            Integer x = list.get(0);
            Integer y = list.get(1);
            int distance = (x - point.get(0)) * (x - point.get(0)) + (y - point.get(1)) * (y - point.get(1));
            distanceList.put(distance, list);
        }

        Integer distance = distanceList.keySet().stream().min(Comparator.naturalOrder()).get();
        return distanceList.get(distance);
    }

    /**
     * 获取可攻击位置
     *
     * @param move        移动距离
     * @param destination 目的地
     * @param view        视图
     * @param zhangAiList 障碍物
     */
    public List<List<Integer>> getAvailableLocation(int move, View view, List<Integer> destination, List<List<Integer>> zhangAiList) {
        List<List<Integer>> moveList = getSameLine(destination, move, view.getRowLen(), view.getColLen());
        List<List<Integer>> result = new ArrayList<>();
        for (List<Integer> list : moveList) {
            if (!zhangAiList.contains(list)) {
                result.add(list);
            }
        }
        return result;
    }

    /**
     * 获取周围坐标
     *
     * @param location 当前位置
     * @param move     平移距离
     * @param rowLen   行高
     * @param colLen   列宽
     */
    public List<List<Integer>> getSameLine(List<Integer> location, int move, int rowLen, int colLen) {
        Integer x = location.get(0);
        Integer y = location.get(1);
        List<List<Integer>> moveList = new ArrayList<>();
        for (int i = 1; i <= move; i++) {
            if (x - move >= 0) {
                moveList.add(Arrays.asList(x - move, y));
            }
            if (x + move <= rowLen) {
                moveList.add(Arrays.asList(x + move, y));
            }
            if (y - move >= 0) {
                moveList.add(Arrays.asList(x, y - move));
            }
            if (y + move < colLen) {
                moveList.add(Arrays.asList(x, y + move));
            }
        }
        return moveList;
    }

    public TankAction blockMove(TankAction tankAction, TankPosition point) {
        logger.info("==============当前位置==============");
        logger.info(JSON.toJSONString(point.getXy()));
        String tankId = point.getTank().gettId();
        int yiDong = point.getTank().getYidong();
        Direct prev1;
        Direct prev2 = null;
        List<TankAction> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(tankPoZhang)) {
            list = tankPoZhang.stream().filter(v -> v.gettId().equals(tankId)).collect(Collectors.toList());
            if (list.size() > 1) {
                prev1 = list.get(1).getDirection();
                prev2 = list.get(0).getDirection();

            } else if (list.size() == 1) {
                prev1 = list.get(0).getDirection();
            } else {
                prev1 = Direct.RIGHT;
                list.add(new TankAction(tankId, prev1));
                tankPoZhang.addAll(list);
            }
        } else {
            tankPoZhang = new ArrayList<>();
            prev1 = Direct.RIGHT;
            list.add(new TankAction(tankId, prev1));
            tankPoZhang.addAll(list);
        }
        tankAction.setType(Type.MOVE);
        if (point.getIsTeamB()) {
            List<String> zhangAi = new ArrayList<>(luZhang);
            zhangAi.addAll(teamB);
            zhangAi.remove(tankId);
            // 上次移动是向右
            if (prev1.equals(Direct.RIGHT) && !Direct.LEFT.equals(prev2)) {
                // 如果右方是障碍物/已经移动到最右方
                if (zhangAi.contains(point.getRight()) || StringUtils.isEmpty(point.getRight())) {
                    // 下方也是障碍物/最左方
                    if (zhangAi.contains(point.getDown()) || StringUtils.isEmpty(point.getDown())) {
                        // 左方也是障碍物/最下方
                        if (zhangAi.contains(point.getLeft()) || StringUtils.isEmpty(point.getLeft())) {
                            // 上方也是我方坦克或者是障碍物/最上方
                            if (zhangAi.contains(point.getUp()) || StringUtils.isEmpty(point.getUp())) {
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                            } else {
                                tankAction.setLength(1);
                                tankAction.setDirection(Direct.UP);
                            }
                        } else {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.LEFT);
                        }
                        // 表示坦克的移动距离>1时是障碍物/最下方
                    } else if (zhangAi.contains(point.getDownInMove()) || StringUtils.isEmpty(point.getDownInMove())) {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.DOWN);
                    } else {
                        tankAction.setLength(yiDong);
                        tankAction.setDirection(Direct.DOWN);
                    }
                    // 移动位置>1有障碍，那么只能移动1
                } else if (zhangAi.contains(point.getRightInMove()) || StringUtils.isEmpty(point.getRightInMove())) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.RIGHT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.RIGHT);
                }
                tankPoZhang.removeAll(list);
                tankPoZhang.add(list.get(list.size() - 1));
                tankPoZhang.add(tankAction);
            } else if (prev1.equals(Direct.LEFT) && !Direct.RIGHT.equals(prev2)) {
                // 如果左方是障碍物/已经移动到最左边
                if (zhangAi.contains(point.getLeft()) || StringUtils.isEmpty(point.getLeft())) {
                    // 下方也是障碍物/最下方
                    if (zhangAi.contains(point.getDown()) || StringUtils.isEmpty(point.getDown())) {
                        // 右方也是障碍物/最右方
                        if (zhangAi.contains(point.getRight()) || StringUtils.isEmpty(point.getRight())) {
                            // 上方也是我方坦克或者是障碍物/最上方
                            if (zhangAi.contains(point.getUp()) || StringUtils.isEmpty(point.getUp())) {
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                            } else {
                                tankAction.setLength(1);
                                tankAction.setDirection(Direct.UP);
                            }
                        }
                        // 向又移动一格
                        else {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.RIGHT);
                        }
                        // 表示坦克的移动距离>1时是障碍物
                    } else if (zhangAi.contains(point.getDownInMove()) || StringUtils.isEmpty(point.getDownInMove())) {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.DOWN);
                    } else {
                        tankAction.setLength(yiDong);
                        tankAction.setDirection(Direct.DOWN);
                    }
                    // 移动位置>1有障碍，那么只能移动1
                } else if (zhangAi.contains(point.getLeftInMove()) || StringUtils.isEmpty(point.getLeftInMove())) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.LEFT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.LEFT);
                }
                tankPoZhang.removeAll(list);
                tankPoZhang.add(list.get(list.size() - 1));
                tankPoZhang.add(tankAction);
                // 上一次是向下
            } else if (prev1.equals(Direct.DOWN)) {
                // 表示右方是障碍物/最右方
                if (zhangAi.contains(point.getRight()) || StringUtils.isEmpty(point.getRight())) {
                    // 表示左方是障碍物/最左方
                    if (zhangAi.contains(point.getLeft()) || StringUtils.isEmpty(point.getLeft())) {
                        // 表示下方是障碍物/最下方
                        if (zhangAi.contains(point.getDown()) || StringUtils.isEmpty(point.getDown())) {
                            // 表示上方是障碍物/最上边
                            if (zhangAi.contains(point.getUp()) || StringUtils.isEmpty(point.getUp())) {
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                            } else {
                                tankAction.setLength(1);
                                tankAction.setDirection(Direct.UP);
                            }
                        } else {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.DOWN);
                        }
                    } else if (zhangAi.contains(point.getLeftInMove()) || StringUtils.isEmpty(point.getLeftInMove())) {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.LEFT);
                    } else {
                        tankAction.setLength(yiDong);
                        tankAction.setDirection(Direct.LEFT);
                    }
                } else if (zhangAi.contains(point.getRightInMove()) || StringUtils.isEmpty(point.getRightInMove())) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.RIGHT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.RIGHT);
                }
                tankPoZhang.removeAll(list);
                tankPoZhang.add(list.get(list.size() - 1));
                tankPoZhang.add(tankAction);

            } else {
                if (prev1.equals(Direct.RIGHT)) {
                    // 表示下方是障碍物/最下方
                    if (zhangAi.contains(point.getDown()) || StringUtils.isEmpty(point.getDown())) {
                        // 表示左方是障碍物/最右方
                        if (!zhangAi.contains(point.getRight()) && !StringUtils.isEmpty(point.getRight())) {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.RIGHT);
                        }
                    } else {
                        if (zhangAi.contains(point.getDownInMove()) || StringUtils.isEmpty(point.getDownInMove())) {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.DOWN);
                        } else {
                            tankAction.setLength(yiDong);
                            tankAction.setDirection(Direct.DOWN);
                        }
                        tankPoZhang.removeAll(list);
                        tankPoZhang.add(list.get(list.size() - 1));
                        tankPoZhang.add(tankAction);
                    }
                } else if (prev1.equals(Direct.LEFT)) {
                    if (zhangAi.contains(point.getDown()) || StringUtils.isEmpty(point.getDown())) {
                        // 表示左方是障碍物/最左
                        if (!zhangAi.contains(point.getLeft()) && !StringUtils.isEmpty(point.getLeft())) {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.LEFT);
                        }
                    } else {
                        if (zhangAi.contains(point.getDownInMove()) || StringUtils.isEmpty(point.getDownInMove())) {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.DOWN);
                        } else {
                            tankAction.setLength(yiDong);
                            tankAction.setDirection(Direct.DOWN);
                        }
                        tankPoZhang.removeAll(list);
                        tankPoZhang.add(list.get(list.size() - 1));
                        tankPoZhang.add(tankAction);
                    }

                }
            }

        }

        if (point.getIsTeamC()) {
            List<String> zhangAi = new ArrayList<>(luZhang);
            zhangAi.addAll(teamC);
            zhangAi.remove(tankId);
            // 上次移动是向右
            if (prev1.equals(Direct.RIGHT) && !Direct.LEFT.equals(prev2)) {
                // 如果右方是障碍物/已经移动到最右方
                if (zhangAi.contains(point.getRight()) || StringUtils.isEmpty(point.getRight())) {
                    // 上方也是障碍物/最上方
                    if (zhangAi.contains(point.getUp()) || StringUtils.isEmpty(point.getUp())) {
                        // 左方也是障碍物/最下方
                        if (zhangAi.contains(point.getLeft()) || StringUtils.isEmpty(point.getLeft())) {
                            // 下方也是我方坦克或者是障碍物/最下方
                            if (zhangAi.contains(point.getDown()) || StringUtils.isEmpty(point.getDown())) {
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                            } else {
                                tankAction.setLength(1);
                                tankAction.setDirection(Direct.DOWN);
                            }
                        } else {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.LEFT);
                        }
                        // 表示坦克的移动距离>1时是障碍物/最上方
                    } else if (zhangAi.contains(point.getUpInMove()) || StringUtils.isEmpty(point.getUpInMove())) {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.UP);
                    } else {
                        tankAction.setLength(yiDong);
                        tankAction.setDirection(Direct.UP);
                    }
                    // 移动位置>1有障碍，那么只能移动1
                } else if (zhangAi.contains(point.getRightInMove()) || StringUtils.isEmpty(point.getRightInMove())) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.RIGHT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.RIGHT);
                }
                tankPoZhang.removeAll(list);
                tankPoZhang.add(list.get(list.size() - 1));
                tankPoZhang.add(tankAction);
            } else if (prev1.equals(Direct.LEFT) && !Direct.RIGHT.equals(prev2)) {
                // 如果左方是障碍物/已经移动到最左边
                if (zhangAi.contains(point.getLeft()) || StringUtils.isEmpty(point.getLeft())) {
                    // 上方也是障碍物/最上方
                    if (zhangAi.contains(point.getUp()) || StringUtils.isEmpty(point.getUp())) {
                        // 右方也是障碍物/最右方
                        if (zhangAi.contains(point.getRight()) || StringUtils.isEmpty(point.getRight())) {
                            // 下方也是我方坦克或者是障碍物/最下方
                            if (zhangAi.contains(point.getDown()) || StringUtils.isEmpty(point.getDown())) {
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                            } else {
                                tankAction.setLength(1);
                                tankAction.setDirection(Direct.DOWN);
                            }
                        }
                        // 向又移动一格
                        else {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.RIGHT);
                        }
                        // 表示坦克的移动距离>1时是障碍物
                    } else if (zhangAi.contains(point.getUpInMove()) || StringUtils.isEmpty(point.getUpInMove())) {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.UP);
                    } else {
                        tankAction.setLength(yiDong);
                        tankAction.setDirection(Direct.UP);
                    }
                    // 移动位置>1有障碍，那么只能移动1
                } else if (zhangAi.contains(point.getLeftInMove()) || StringUtils.isEmpty(point.getLeftInMove())) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.LEFT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.LEFT);
                }
                tankPoZhang.removeAll(list);
                tankPoZhang.add(list.get(list.size() - 1));
                tankPoZhang.add(tankAction);
                // 上一次是向上
            } else if (prev1.equals(Direct.UP)) {
                // 表示左方是障碍物/最左边
                if (zhangAi.contains(point.getLeft()) || StringUtils.isEmpty(point.getLeft())) {
                    // 表示右边是障碍物/最右边
                    if (zhangAi.contains(point.getRight()) || StringUtils.isEmpty(point.getRight())) {
                        // 表示上方是障碍物/最上方
                        if (zhangAi.contains(point.getUp()) || StringUtils.isEmpty(point.getUp())) {
                            // 表示下方是障碍物/最下方
                            if (zhangAi.contains(point.getDown()) || StringUtils.isEmpty(point.getDown())) {
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                            } else {
                                tankAction.setLength(1);
                                tankAction.setDirection(Direct.DOWN);
                            }
                        } else {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.UP);
                        }
                    } else if (zhangAi.contains(point.getRightInMove()) || StringUtils.isEmpty(point.getRightInMove())) {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.RIGHT);
                    } else {
                        tankAction.setLength(yiDong);
                        tankAction.setDirection(Direct.RIGHT);
                    }
                } else if (zhangAi.contains(point.getLeftInMove()) || StringUtils.isEmpty(point.getLeftInMove())) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.LEFT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.LEFT);
                }
                tankPoZhang.removeAll(list);
                tankPoZhang.add(list.get(list.size() - 1));
                tankPoZhang.add(tankAction);

            } else {
                if (prev1.equals(Direct.RIGHT)) {
                    if (zhangAi.contains(point.getUp()) || StringUtils.isEmpty(point.getUp())) {
                        if (!zhangAi.contains(point.getRight()) && !StringUtils.isEmpty(point.getRight())) {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.RIGHT);
                        }
                    } else {
                        if (zhangAi.contains(point.getUpInMove()) || StringUtils.isEmpty(point.getUpInMove())) {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.UP);
                        } else {
                            tankAction.setLength(yiDong);
                            tankAction.setDirection(Direct.UP);
                        }
                        tankPoZhang.removeAll(list);
                        tankPoZhang.add(list.get(list.size() - 1));
                        tankPoZhang.add(tankAction);
                    }
                } else if (prev1.equals(Direct.LEFT)) {
                    if (zhangAi.contains(point.getUp()) || StringUtils.isEmpty(point.getUp())) {
                        if (!zhangAi.contains(point.getLeft()) && !StringUtils.isEmpty(point.getLeft())) {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.LEFT);
                        }
                    } else {
                        if (zhangAi.contains(point.getUpInMove()) || StringUtils.isEmpty(point.getUpInMove())) {
                            tankAction.setLength(1);
                            tankAction.setDirection(Direct.UP);
                        } else {
                            tankAction.setLength(yiDong);
                            tankAction.setDirection(Direct.UP);
                        }
                        tankPoZhang.removeAll(list);
                        tankPoZhang.add(list.get(list.size() - 1));
                        tankPoZhang.add(tankAction);
                    }

                }
            }

        }

        logger.info("==============移动目标==============");
        logger.info(JSON.toJSONString(tankAction));
        return tankAction;
    }


    @Deprecated
    public TankAction move(TankAction tankAction, View view, TankPosition tankPosition, List<Integer> destination, List<String> zhangAi) {
        String tankId = tankPosition.getTank().gettId();
        List<String> fastTank = Arrays.asList("C3", "C4", "B3", "B4");
        if (fastTank.contains(tankId)) {
            return moveTo(tankAction, tankPosition, destination, zhangAi);
        }
        List<Integer> position = tankPosition.getXy();
        Integer positionX = position.get(0);
        Integer positionY = position.get(1);
        Integer destinationX = destination.get(0);
        Integer destinationY = destination.get(1);
        if (StringUtils.isEmpty(position) || StringUtils.isEmpty(destination)) {
            return moveTo(tankAction, tankPosition, destination, zhangAi);
        }
        int moveL = moveLength(view, position, Direct.LEFT, zhangAi);
        int moveU = moveLength(view, position, Direct.UP, zhangAi);
        int moveD = moveLength(view, position, Direct.DOWN, zhangAi);
        int moveR = moveLength(view, position, Direct.RIGHT, zhangAi);
        List<TankAction> list1 = null;
        Direct prev1 = null;
        if (!CollectionUtils.isEmpty(tankMove)) {
            list1 = tankMove.stream().filter(v -> v.gettId().equals(tankId)).collect(Collectors.toList());
            if (list1.size() >= 1) {
                prev1 = list1.get(0).getDirection();

            }
        } else {
            tankMove = new ArrayList<>();
        }
        tankAction.setType(Type.MOVE);
        if (positionY > destinationY && !Direct.RIGHT.equals(prev1)) {
            // 表示左边不能移动
            if (moveL == 0) {
                if (positionX > destinationX && !Direct.DOWN.equals(prev1)) {
                    // 表示上方没有移动空间
                    if (moveU == 0) {
                        // 表示下方没有移动空间
                        if (moveD == 0) {
                            if (moveR == 0) {
                                // 表示右方也不能移动
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.WAIT);
                            } else {
                                // 右方有移动空间
                                for (int i = 1; i <= moveR; i++) {
                                    List<Integer> list = Arrays.asList(positionX, positionY + i);
                                    // 向右移动到模有某个位置后上方没有障碍物
                                    if (!hasZhangAi(view, list, Direct.UP, zhangAi)) {
                                        tankAction.setLength(1);
                                        tankAction.setDirection(Direct.RIGHT);
                                    }
                                }
                                // 向右移动到任何位置都有障碍物
                            }
                            // 可以向下移动
                        } else {
                            for (int i = 1; i <= moveD; i++) {
                                List<Integer> list = Arrays.asList(positionX + i, positionY);
                                // 向下移动到模有某个位置后左方没有障碍物
                                if (!hasZhangAi(view, list, Direct.LEFT, zhangAi)) {
                                    tankAction.setDirection(Direct.DOWN);
                                    tankAction.setLength(1);
                                }
                            }
                            // 向下移动到任何位置都有障碍物
                        }

                    } else {
                        for (int i = 1; i <= moveU; i++) {
                            List<Integer> list = Arrays.asList(positionX - i, positionY);
                            // 向上移动到某个位置后左方没有障碍物
                            if (!hasZhangAi(view, list, Direct.LEFT, zhangAi)) {
                                tankAction.setDirection(Direct.UP);
                                tankAction.setLength(1);
                            }
                        }
                    }
                }
                if (positionX < destinationX && !Direct.UP.equals(prev1)) {
                    // 表示下方没有移动空间
                    if (moveD == 0) {
                        // 表示上方没有移动空间
                        if (moveU == 0) {
                            if (moveR == 0) {
                                // 表示右方也不能移动
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.WAIT);
                            } else {
                                // 右方有移动空间
                                for (int i = 1; i <= moveR; i++) {
                                    List<Integer> list = Arrays.asList(positionX, positionY + i);
                                    // 向右移动到模有某个位置后下方没有障碍物
                                    if (!hasZhangAi(view, list, Direct.DOWN, zhangAi)) {
                                        tankAction.setLength(1);
                                        tankAction.setDirection(Direct.RIGHT);
                                    }
                                }
                                // 向右移动到任何位置都有障碍物
                            }
                            // 可以向上移动
                        } else {
                            for (int i = 1; i <= moveU; i++) {
                                List<Integer> list = Arrays.asList(positionX - i, positionY);
                                // 向上移动到某个位置后左方没有障碍物
                                if (!hasZhangAi(view, list, Direct.LEFT, zhangAi)) {
                                    tankAction.setDirection(Direct.UP);
                                    tankAction.setLength(1);
                                }
                            }
                            // 向上移动到任何位置都有障碍物
                        }

                    } else {
                        for (int i = 1; i <= moveD; i++) {
                            List<Integer> list = Arrays.asList(positionX + i, positionY);
                            // 向下移动到某个位置后左方没有障碍物
                            if (!hasZhangAi(view, list, Direct.LEFT, zhangAi)) {
                                tankAction.setDirection(Direct.DOWN);
                                tankAction.setLength(1);
                            }
                        }
                    }
                }

            } else {
                if (positionX > destinationX) {
                    // 可以向左移动
                    for (int i = 0; i < moveL; i++) {
                        List<Integer> list = Arrays.asList(positionX, positionY - i);
                        // 向左移动到某个位置后后上方没有障碍物
                        if (!hasZhangAi(view, list, Direct.UP, zhangAi)) {
                            tankAction.setDirection(Direct.LEFT);
                            tankAction.setLength(1);
                        }
                    }
                }
                if (positionX < destinationX) {
                    // 可以向左移动
                    for (int i = 0; i < moveL; i++) {
                        List<Integer> list = Arrays.asList(positionX, positionY - i);
                        // 向左移动到某个位置后后下方没有障碍物
                        if (!hasZhangAi(view, list, Direct.DOWN, zhangAi)) {
                            tankAction.setDirection(Direct.LEFT);
                            tankAction.setLength(1);
                        }
                    }
                }
            }
        }
        if (positionY < destinationY && !Direct.LEFT.equals(prev1)) {
            // 表示右边不能移动
            if (moveR == 0) {
                if (positionX > destinationX && !Direct.DOWN.equals(prev1)) {
                    // 表示上方没有移动空间
                    if (moveU == 0) {
                        // 表示下方没有移动空间
                        if (moveD == 0) {
                            if (moveL == 0) {
                                // 表示左方也不能移动
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.WAIT);
                            } else {
                                // 左方有移动空间
                                for (int i = 1; i <= moveL; i++) {
                                    List<Integer> list = Arrays.asList(positionX, positionY - i);
                                    // 向左移动到模有某个位置后上方没有障碍物
                                    if (!hasZhangAi(view, list, Direct.UP, zhangAi)) {
                                        tankAction.setLength(1);
                                        tankAction.setDirection(Direct.LEFT);
                                    }
                                }
                                // 向左移动到任何位置都有障碍物
                            }
                            // 可以向下移动
                        } else {
                            for (int i = 1; i <= moveD; i++) {
                                List<Integer> list = Arrays.asList(positionX + i, positionY);
                                // 向下移动到模有某个位置后右方没有障碍物
                                if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                                    tankAction.setDirection(Direct.DOWN);
                                    tankAction.setLength(1);
                                }
                            }
                            // 向下移动到任何位置都有障碍物
                        }

                    } else {
                        for (int i = 1; i <= moveU; i++) {
                            List<Integer> list = Arrays.asList(positionX - i, positionY);
                            // 向上移动到某个位置后右方没有障碍物
                            if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                                tankAction.setDirection(Direct.UP);
                                tankAction.setLength(1);
                            }
                        }
                    }
                }
                if (positionX < destinationX && !Direct.UP.equals(prev1)) {
                    // 表示下方没有移动空间
                    if (moveD == 0) {
                        // 表示上方没有移动空间
                        if (moveU == 0) {
                            if (moveL == 0) {
                                // 表示左方也不能移动
                                tankAction.setLength(0);
                                tankAction.setDirection(Direct.WAIT);
                            } else {
                                // 左方有移动空间
                                for (int i = 1; i <= moveL; i++) {
                                    List<Integer> list = Arrays.asList(positionX, positionY - i);
                                    // 向右移动到模有某个位置后下方没有障碍物
                                    if (!hasZhangAi(view, list, Direct.DOWN, zhangAi)) {
                                        tankAction.setLength(1);
                                        tankAction.setDirection(Direct.LEFT);
                                    }
                                }
                                // 向左移动到任何位置都有障碍物
                            }
                            // 可以向上移动
                        } else {
                            for (int i = 1; i <= moveU; i++) {
                                List<Integer> list = Arrays.asList(positionX - i, positionY);
                                // 向上移动到某个位置后右方没有障碍物
                                if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                                    tankAction.setDirection(Direct.UP);
                                    tankAction.setLength(1);
                                }
                            }
                            // 向上移动到任何位置都有障碍物
                        }

                    } else {
                        for (int i = 1; i <= moveD; i++) {
                            List<Integer> list = Arrays.asList(positionX + i, positionY);
                            // 向下移动到某个位置后右方没有障碍物
                            if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                                tankAction.setDirection(Direct.DOWN);
                                tankAction.setLength(1);
                            }
                        }
                    }
                }

            } else {
                if (positionX > destinationX) {
                    // 可以向右移动
                    for (int i = 0; i < moveL; i++) {
                        List<Integer> list = Arrays.asList(positionX, positionY + i);
                        // 向左右移动到某个位置后后上方没有障碍物
                        if (!hasZhangAi(view, list, Direct.UP, zhangAi)) {
                            tankAction.setDirection(Direct.RIGHT);
                            tankAction.setLength(1);
                        }
                    }
                }
                if (positionX < destinationX) {
                    // 可以向右移动
                    for (int i = 0; i < moveL; i++) {
                        List<Integer> list = Arrays.asList(positionX, positionY + i);
                        // 向右移动到某个位置后后下方没有障碍物
                        if (!hasZhangAi(view, list, Direct.DOWN, zhangAi)) {
                            tankAction.setDirection(Direct.RIGHT);
                            tankAction.setLength(1);
                        }
                    }
                }
            }
        }
        if (Direct.RIGHT.equals(prev1) || Direct.LEFT.equals(prev1)) {
            if (positionX > destinationX) {
                // 表示上方没有移动空间
                if (moveU == 0) {
                    // 表示下方没有移动空间
                    if (moveD == 0) {
                        tankAction.setLength(0);
                        tankAction.setDirection(Direct.WAIT);
                    } else {
                        for (int i = 1; i <= moveD; i++) {
                            List<Integer> list = Arrays.asList(positionX + i, positionY);
                            // 向下移动到模有某个位置后右方没有障碍物
                            if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                                tankAction.setDirection(Direct.DOWN);
                                tankAction.setLength(1);
                            }
                        }
                        // 向下移动到任何位置都有障碍物
                    }

                } else {
                    for (int i = 1; i <= moveU; i++) {
                        List<Integer> list = Arrays.asList(positionX - i, positionY);
                        // 向上移动到某个位置后右方没有障碍物
                        if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                            tankAction.setDirection(Direct.UP);
                            tankAction.setLength(1);
                        }
                    }
                }
            }
            if (positionX < destinationX) {
                // 表示下方没有移动空间
                if (moveD == 0) {
                    // 表示上方没有移动空间
                    if (moveU == 0) {

                        tankAction.setLength(0);
                        tankAction.setDirection(Direct.WAIT);
                    } else {
                        for (int i = 1; i <= moveU; i++) {
                            List<Integer> list = Arrays.asList(positionX - i, positionY);
                            // 向上移动到某个位置后右方没有障碍物
                            if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                                tankAction.setDirection(Direct.UP);
                                tankAction.setLength(1);
                            }
                        }
                        // 向上移动到任何位置都有障碍物
                    }

                } else {
                    for (int i = 1; i <= moveD; i++) {
                        List<Integer> list = Arrays.asList(positionX + i, positionY);
                        // 向下移动到某个位置后右方没有障碍物
                        if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                            tankAction.setDirection(Direct.DOWN);
                            tankAction.setLength(1);
                        }
                    }
                }
            }
        }
        if (Direct.UP.equals(prev1) || Direct.DOWN.equals(prev1)) {
            // todo
            if (positionY > destinationY) {
                // 表示上方没有移动空间
                if (moveU == 0) {
                    // 表示下方没有移动空间
                    if (moveD == 0) {
                        tankAction.setLength(0);
                        tankAction.setDirection(Direct.WAIT);
                    } else {
                        for (int i = 1; i <= moveD; i++) {
                            List<Integer> list = Arrays.asList(positionX + i, positionY);
                            // 向下移动到模有某个位置后右方没有障碍物
                            if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                                tankAction.setDirection(Direct.DOWN);
                                tankAction.setLength(1);
                            }
                        }
                        // 向下移动到任何位置都有障碍物
                    }

                } else {
                    for (int i = 1; i <= moveU; i++) {
                        List<Integer> list = Arrays.asList(positionX - i, positionY);
                        // 向上移动到某个位置后右方没有障碍物
                        if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                            tankAction.setDirection(Direct.UP);
                            tankAction.setLength(1);
                        }
                    }
                }
            }
            if (positionX < destinationX) {
                // 表示下方没有移动空间
                if (moveD == 0) {
                    // 表示上方没有移动空间
                    if (moveU == 0) {

                        tankAction.setLength(0);
                        tankAction.setDirection(Direct.WAIT);
                    } else {
                        for (int i = 1; i <= moveU; i++) {
                            List<Integer> list = Arrays.asList(positionX - i, positionY);
                            // 向上移动到某个位置后右方没有障碍物
                            if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                                tankAction.setDirection(Direct.UP);
                                tankAction.setLength(1);
                            }
                        }
                        // 向上移动到任何位置都有障碍物
                    }

                } else {
                    for (int i = 1; i <= moveD; i++) {
                        List<Integer> list = Arrays.asList(positionX + i, positionY);
                        // 向下移动到某个位置后右方没有障碍物
                        if (!hasZhangAi(view, list, Direct.RIGHT, zhangAi)) {
                            tankAction.setDirection(Direct.DOWN);
                            tankAction.setLength(1);
                        }
                    }
                }
            }
        }
        if (CollectionUtils.isEmpty(list1)) {
            tankMove.add(tankAction);
        } else {
            tankMove.removeAll(list1);
            tankMove.add(tankAction);
        }
        if (tankAction.getDirection() == null) {
            tankAction = moveTo(tankAction, tankPosition, destination, zhangAi);
        }
        return tankAction;
    }


    @Deprecated
    public boolean hasZhangAi(View view, List<Integer> position, Direct direct, List<String> zhangAi) {
        List<List<String>> map = view.getMap();
        int colLen = view.getColLen();
        int rowLen = view.getRowLen();
        Integer positionX = position.get(0);
        Integer positionY = position.get(1);
        if (StringUtils.isEmpty(position) || direct == null) {
            return false;
        }
        switch (direct) {
            case UP:
                for (int i = 1; i <= positionX; i++) {
                    List<Integer> list = Arrays.asList(positionX - i, positionY);
                    String type = getTypeByLocation(map, list);
                    if (zhangAi.contains(type)) {
                        return true;
                    }
                }
                return false;
            case DOWN:
                for (int i = 1; i <= rowLen - 1 - positionX; i++) {
                    List<Integer> list = Arrays.asList(positionX + i, positionY);
                    String type = getTypeByLocation(map, list);
                    if (zhangAi.contains(type)) {
                        return true;
                    }
                }
                return false;
            case LEFT:
                for (int i = 1; i <= positionY; i++) {
                    List<Integer> list = Arrays.asList(positionX, positionY - i);
                    String type = getTypeByLocation(map, list);
                    if (zhangAi.contains(type)) {
                        return true;
                    }
                }
                return false;
            case RIGHT:
                for (int i = 1; i <= colLen - 1 - positionY; i++) {
                    List<Integer> list = Arrays.asList(positionX, positionY + i);
                    String type = getTypeByLocation(map, list);
                    if (zhangAi.contains(type)) {
                        return true;
                    }
                }
                return false;
            default:
                return true;
        }

    }

    @Deprecated
    public int moveLength(View view, List<Integer> position, Direct direct, List<String> zhangAi) {
        List<List<String>> map = view.getMap();
        int colIndex = view.getColLen() - 1;
        int rowIndex = view.getRowLen() - 1;
        Integer positionX = position.get(0);
        Integer positionY = position.get(1);
        if (StringUtils.isEmpty(position) || direct == null) {
            return 0;
        }
        switch (direct) {
            case UP:
                for (int i = 1; i <= positionX; i++) {
                    List<Integer> list = Arrays.asList(positionX - i, positionY);
                    String type = getTypeByLocation(map, list);
                    if (zhangAi.contains(type)) {
                        return i - 1;
                    }
                }
                return positionX;
            case DOWN:
                for (int i = 1; i <= rowIndex - positionX; i++) {
                    List<Integer> list = Arrays.asList(positionX + i, positionY);
                    String type = getTypeByLocation(map, list);
                    if (zhangAi.contains(type)) {
                        return i - 1;
                    }
                }
                return rowIndex - positionX;
            case LEFT:
                for (int i = 1; i <= positionY; i++) {
                    List<Integer> list = Arrays.asList(positionX, positionY - i);
                    String type = getTypeByLocation(map, list);
                    if (zhangAi.contains(type)) {
                        return i - 1;
                    }
                }
                return positionY;
            case RIGHT:
                for (int i = 1; i <= colIndex - positionY; i++) {
                    List<Integer> list = Arrays.asList(positionX, positionY + i);
                    String type = getTypeByLocation(map, list);
                    if (zhangAi.contains(type)) {
                        return i - 1;
                    }
                }
                return colIndex - positionY;
            default:
                return 0;
        }
    }

    /**
     * 移动
     *
     * @param tankAction   坦克操作
     * @param tankPosition 坦克坐标
     * @param destination  目的地
     * @param zhangAi      路障
     */
    @Deprecated
    public TankAction moveTo(TankAction tankAction, TankPosition tankPosition, List<Integer> destination, List<String> zhangAi) {
        int yiDong = tankPosition.getTank().getYidong();
        String tankId = tankPosition.getTank().gettId();
        Integer tankX = tankPosition.getXy().get(0);
        Integer tankY = tankPosition.getXy().get(1);
        if (CollectionUtils.isEmpty(destination)) {
            return blockMove(tankAction, tankPosition);
        }
        Integer destinationX = destination.get(0);
        Integer destinationY = destination.get(1);
        List<TankAction> list = null;
        Direct prev1 = null;
        if (!CollectionUtils.isEmpty(tankMove2)) {
            list = tankMove2.stream().filter(v -> v.gettId().equals(tankId)).collect(Collectors.toList());
            if (list.size() >= 1) {
                prev1 = list.get(0).getDirection();

            }
        } else {
            tankMove2 = new ArrayList<>();
        }
        tankAction.setType(Type.MOVE);

        if (destinationX < tankX && destinationY <= tankY && !Direct.DOWN.equals(prev1)) {
            if (zhangAi.contains(tankPosition.getUp()) || StringUtils.isEmpty(tankPosition.getUp())) {
                if (zhangAi.contains(tankPosition.getLeft()) || StringUtils.isEmpty(tankPosition.getLeft())) {
                    if (zhangAi.contains(tankPosition.getDown()) || StringUtils.isEmpty(tankPosition.getDown())) {
                        if (zhangAi.contains(tankPosition.getRight()) || StringUtils.isEmpty(tankPosition.getRight())) {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                        } else {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.RIGHT);
                        }
                    } else {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.DOWN);
                    }

                } else if (zhangAi.contains(tankPosition.getLeftInMove()) || StringUtils.isEmpty(tankPosition.getLeftInMove()) || tankY - yiDong < destinationY) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.LEFT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.LEFT);
                }

            } else if (zhangAi.contains(tankPosition.getUpInMove()) || StringUtils.isEmpty(tankPosition.getUpInMove()) || tankX - yiDong < destinationX) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.UP);
            } else {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.UP);
            }
        } else if (destinationX <= tankX && destinationY > tankY && !Direct.LEFT.equals(prev1)) {
            if (zhangAi.contains(tankPosition.getRight()) || StringUtils.isEmpty(tankPosition.getRight())) {
                if (zhangAi.contains(tankPosition.getUp()) || StringUtils.isEmpty(tankPosition.getUp())) {
                    if (zhangAi.contains(tankPosition.getLeft()) || StringUtils.isEmpty(tankPosition.getLeft())) {
                        if (zhangAi.contains(tankPosition.getDown()) || StringUtils.isEmpty(tankPosition.getDown())) {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                        } else {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.DOWN);
                        }
                    } else {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.LEFT);
                    }

                } else if (zhangAi.contains(tankPosition.getUpInMove()) || StringUtils.isEmpty(tankPosition.getUpInMove()) || tankX - yiDong < destinationX) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.UP);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.UP);
                }

            } else if (zhangAi.contains(tankPosition.getRightInMove()) || StringUtils.isEmpty(tankPosition.getRightInMove()) || tankY + yiDong > destinationY) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.RIGHT);
            } else {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.RIGHT);
            }
        }

        if (destinationX >= tankX && destinationY < tankY && !Direct.RIGHT.equals(prev1)) {
            if (zhangAi.contains(tankPosition.getLeft()) || StringUtils.isEmpty(tankPosition.getLeft())) {
                if (zhangAi.contains(tankPosition.getDown()) || StringUtils.isEmpty(tankPosition.getDown())) {
                    if (zhangAi.contains(tankPosition.getRight()) || StringUtils.isEmpty(tankPosition.getRight())) {
                        if (zhangAi.contains(tankPosition.getUp()) || StringUtils.isEmpty(tankPosition.getUp())) {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                        } else {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.UP);
                        }
                    } else {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.RIGHT);
                    }

                } else if (zhangAi.contains(tankPosition.getDownInMove()) || StringUtils.isEmpty(tankPosition.getDownInMove()) || tankX + yiDong > destinationX) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.DOWN);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.DOWN);
                }

            } else if (zhangAi.contains(tankPosition.getLeftInMove()) || StringUtils.isEmpty(tankPosition.getLeftInMove()) || tankY - yiDong > destinationY) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.LEFT);
            } else {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.LEFT);
            }
        } else if (destinationX > tankX && destinationY >= tankY && !Direct.UP.equals(prev1)) {
            if (zhangAi.contains(tankPosition.getDown()) || StringUtils.isEmpty(tankPosition.getDown())) {
                if (zhangAi.contains(tankPosition.getRight()) || StringUtils.isEmpty(tankPosition.getRight())) {
                    if (zhangAi.contains(tankPosition.getUp()) || StringUtils.isEmpty(tankPosition.getUp())) {
                        if (zhangAi.contains(tankPosition.getLeft()) || StringUtils.isEmpty(tankPosition.getLeft())) {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                        } else {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.LEFT);
                        }
                    } else {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.UP);
                    }

                } else if (zhangAi.contains(tankPosition.getRightInMove()) || StringUtils.isEmpty(tankPosition.getRightInMove()) || tankY + yiDong > destinationY) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.RIGHT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.RIGHT);
                }

            } else if (zhangAi.contains(tankPosition.getDownInMove()) || StringUtils.isEmpty(tankPosition.getDownInMove()) || tankX + yiDong > destinationX) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.DOWN);
            } else {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.DOWN);
            }
        } else if (destinationX <= tankX && destinationY < tankY && Direct.DOWN.equals(prev1)) {
            if (zhangAi.contains(tankPosition.getLeft()) || StringUtils.isEmpty(tankPosition.getLeft())) {
                if (zhangAi.contains(tankPosition.getUp()) || StringUtils.isEmpty(tankPosition.getUp())) {
                    if (zhangAi.contains(tankPosition.getRight()) || StringUtils.isEmpty(tankPosition.getRight())) {
                        if (zhangAi.contains(tankPosition.getDown()) || StringUtils.isEmpty(tankPosition.getDown())) {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                        } else {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.DOWN);
                        }
                    } else {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.RIGHT);
                    }

                } else if (zhangAi.contains(tankPosition.getUpInMove()) || StringUtils.isEmpty(tankPosition.getUpInMove()) || tankX - yiDong < destinationX) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.UP);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.UP);
                }

            } else if (zhangAi.contains(tankPosition.getLeftInMove()) || StringUtils.isEmpty(tankPosition.getLeftInMove()) || tankY - yiDong < destinationY) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.LEFT);
            } else {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.LEFT);
            }
        } else if (destinationX < tankX && destinationY >= tankY && Direct.LEFT.equals(prev1)) {
            if (zhangAi.contains(tankPosition.getUp()) || StringUtils.isEmpty(tankPosition.getUp())) {
                if (zhangAi.contains(tankPosition.getRight()) || StringUtils.isEmpty(tankPosition.getRight())) {
                    if (zhangAi.contains(tankPosition.getDown()) || StringUtils.isEmpty(tankPosition.getDown())) {
                        if (zhangAi.contains(tankPosition.getLeft()) || StringUtils.isEmpty(tankPosition.getLeft())) {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                        } else {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.LEFT);
                        }
                    } else {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.DOWN);
                    }

                } else if (zhangAi.contains(tankPosition.getRightInMove()) || StringUtils.isEmpty(tankPosition.getRightInMove()) || tankY + yiDong > destinationX) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.RIGHT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.RIGHT);
                }

            } else if (zhangAi.contains(tankPosition.getUpInMove()) || StringUtils.isEmpty(tankPosition.getUpInMove()) || tankX - yiDong < destinationX) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.UP);
            } else {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.UP);
            }
        }

        if (destinationX > tankX && destinationY <= tankY && Direct.RIGHT.equals(prev1)) {
            if (zhangAi.contains(tankPosition.getDown()) || StringUtils.isEmpty(tankPosition.getDown())) {
                if (zhangAi.contains(tankPosition.getRight()) || StringUtils.isEmpty(tankPosition.getRight())) {
                    if (zhangAi.contains(tankPosition.getUp()) || StringUtils.isEmpty(tankPosition.getUp())) {
                        if (zhangAi.contains(tankPosition.getLeft()) || StringUtils.isEmpty(tankPosition.getLeft())) {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                        } else {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.LEFT);
                        }
                    } else {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.UP);
                    }

                } else if (zhangAi.contains(tankPosition.getRightInMove()) || StringUtils.isEmpty(tankPosition.getRightInMove()) || tankY + yiDong > destinationY) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.RIGHT);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.RIGHT);
                }

            } else if (zhangAi.contains(tankPosition.getDownInMove()) || StringUtils.isEmpty(tankPosition.getDownInMove()) || tankX + yiDong > destinationX) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.DOWN);
            } else {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.DOWN);
            }
        } else if (destinationX >= tankX && destinationY > tankY && Direct.UP.equals(prev1)) {
            if (zhangAi.contains(tankPosition.getRight()) || StringUtils.isEmpty(tankPosition.getRight())) {
                if (zhangAi.contains(tankPosition.getUp()) || StringUtils.isEmpty(tankPosition.getUp())) {
                    if (zhangAi.contains(tankPosition.getLeft()) || StringUtils.isEmpty(tankPosition.getLeft())) {
                        if (zhangAi.contains(tankPosition.getDown()) || StringUtils.isEmpty(tankPosition.getDown())) {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                        } else {
                            tankAction.setLength(0);
                            tankAction.setDirection(Direct.DOWN);
                        }
                    } else {
                        tankAction.setLength(1);
                        tankAction.setDirection(Direct.LEFT);
                    }

                } else if (zhangAi.contains(tankPosition.getUpInMove()) || StringUtils.isEmpty(tankPosition.getUpInMove()) || tankX - yiDong < destinationX) {
                    tankAction.setLength(1);
                    tankAction.setDirection(Direct.UP);
                } else {
                    tankAction.setLength(yiDong);
                    tankAction.setDirection(Direct.UP);
                }

            } else if (zhangAi.contains(tankPosition.getRightInMove()) || StringUtils.isEmpty(tankPosition.getRightInMove()) || tankY + yiDong > destinationY) {
                tankAction.setLength(1);
                tankAction.setDirection(Direct.RIGHT);
            } else {
                tankAction.setLength(yiDong);
                tankAction.setDirection(Direct.RIGHT);
            }
        }
        if (CollectionUtils.isEmpty(list)) {
            tankMove2.add(tankAction);
        } else {
            tankMove2.removeAll(list);
            tankMove2.add(tankAction);
        }


        return tankAction;
    }

    /**
     * 开火
     *
     * @param tankAction     坦克操作
     * @param attackDistance 攻击记录
     * @param tank           坦克坐标
     * @param diFangTank     地方坦克左边
     */
    public void fire(TankAction tankAction, int attackDistance, List<Integer> tank, List<Integer> diFangTank) {
        Integer tankX = tank.get(0);
        Integer tankY = tank.get(1);
        Integer diFangTankX = diFangTank.get(0);
        Integer diFangTankY = diFangTank.get(1);
        tankAction.setType(Type.FIRE);
        if (tankX.equals(diFangTankX)) {
            if (tankY < diFangTankY) {
                if (diFangTankY - tankY >= 1 && diFangTankY - tankY <= attackDistance) {
                    tankAction.setDirection(Direct.RIGHT);
                    tankAction.setLength(diFangTankY - tankY);
                }
            } else {
                if (tankY - diFangTankY >= 1 && tankY - diFangTankY <= attackDistance) {
                    tankAction.setDirection(Direct.LEFT);
                    tankAction.setLength(tankY - diFangTankY);
                }
            }
        } else {
            if (tankY.equals(diFangTankY)) {
                if (tankX < diFangTankX) {
                    if (diFangTankX - tankX >= 1 && diFangTankX - tankX <= attackDistance) {
                        tankAction.setDirection(Direct.DOWN);
                        tankAction.setLength(diFangTankX - tankX);
                    }
                } else {
                    if (tankX - diFangTankX >= 1 && tankX - diFangTankX <= attackDistance) {
                        tankAction.setDirection(Direct.UP);
                        tankAction.setLength(tankX - diFangTankX);
                    }
                }
            }
        }
    }

}
