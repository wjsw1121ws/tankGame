package com.source3g.platform.service;

import com.alibaba.fastjson2.JSON;
import com.source3g.platform.contants.Direct;
import com.source3g.platform.contants.Type;
import com.source3g.platform.dto.*;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.source3g.platform.contants.Constant.TEAM_B;
import static com.source3g.platform.contants.Type.MOVE;

/**
 * 业务层
 *
 * @author W10005176
 */
@Service
public class PlayerService {
    Map<String, Direct> behindDirection;

    /**
     * * 注意 对于glod值：
     * 返回的操作列表中包含已死亡的坦克时，自动扣除复活币
     * 每个复活币可复活一辆坦克
     * 如果返回的操作列表中死亡的坦克超过复活币数量，自动按顺序复活
     * * 注意 对于tId值的范围：
     * 如果被分配为B队，那tId的值的范围为("B1","B2","B3","B4","B5")
     * 如果被分配为C队，那tId的值的范围为("C1","C2","C3","C4","C5")
     * 如果tId返回不满足要求，服务器会拒收
     * * 注意 对于length值的范围：
     * 攻击或者移动的距离不能超过该坦克的能力，否则会被丢弃
     */
    public List<TankAction> action(GameMap gameMap) {
        List<TankAction> tankActionList = new ArrayList<>();
        String team = gameMap.getTeam();

        // B队
        if (TEAM_B.equals(team)) {
            TankGroup tankGroupB = gameMap.gettB();
            List<Tank> tanks = tankGroupB.getTanks();
            for (Tank tank : tanks) {

                TankAction tankAction = new TankAction();
                tankAction.settId(tank.gettId());

                tankAction = new PlayerCommon().operate(tankAction, tank, gameMap.getView());

                if (tankAction.getDirection() == null || tankAction.getType() == null) {
                    tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                    tankAction.setType(Type.values()[new Random().nextInt(2)]);
                    if (tankAction.getType().equals(MOVE)) {
                        tankAction.setLength(new Random().nextInt(tank.getYidong() + 1));
                    } else {
                        tankAction.setLength(new Random().nextInt(tank.getShecheng() + 1));
                    }
                }
                if ("B1".equals(tank.gettId())) {
                    tankAction.setUseGlod(true);
                } else {
                    tankAction.setUseGlod(false);
                }
                tankActionList.add(tankAction);
            }

            // C队
        } else {
            TankGroup tankGroupC = gameMap.gettC();
            List<Tank> tanks = tankGroupC.getTanks();
            for (Tank tank : tanks) {
                /*TankAction tankAction = new TankAction();
                if (tank.getShengyushengming() == 0) {
                    // 已经牺牲了，如果要移动就需要消耗复活币
                    tankAction.setUseGlod(false);
                    continue;
                }
                tankAction.settId(tank.gettId());
                tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                tankAction.setType(Type.values()[new Random().nextInt(2)]);
                if (tankAction.getType().equals(MOVE)) {
                    tankAction.setLength(new Random().nextInt(tank.getYidong() + 1));
                } else {
                    tankAction.setLength(new Random().nextInt(tank.getShecheng() + 1));
                }
                tankActionList.add(tankAction);*/
                TankAction tankAction = new TankAction();
                tankAction.settId(tank.gettId());

                tankAction = new PlayerCommon().operate(tankAction, tank, gameMap.getView());

                if (tankAction.getDirection() == null || tankAction.getType() == null) {
                    tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
                    tankAction.setType(Type.values()[new Random().nextInt(2)]);
                    if (tankAction.getType().equals(MOVE)) {
                        tankAction.setLength(new Random().nextInt(tank.getYidong() + 1));
                    } else {
                        tankAction.setLength(new Random().nextInt(tank.getShecheng() + 1));
                    }
                }
                if ("C1".equals(tank.gettId())) {
                    tankAction.setUseGlod(true);
                } else {
                    tankAction.setUseGlod(false);
                }
                tankActionList.add(tankAction);
            }
        }
        return tankActionList;
    }

    public TankAction run(Tank tank, View view) {
        TankAction tankAction = new TankAction();
        // 获取当前坦克的坐标
        List<List<String>> tankLocation = getLocationsByTypes(view.getMap(), Collections.singletonList(tank.gettId()));
        // 获取路障的坐标

        if (tank.getShengyushengming() == 0) {
            // 已经牺牲了，如果要移动就需要消耗复活币
            tankAction.setUseGlod(true);
        }
        tankAction.settId(tank.gettId());
        tankAction.setDirection(Direct.values()[new Random().nextInt(4)]);
        tankAction.setType(Type.values()[new Random().nextInt(2)]);
        if (tankAction.getType().equals(MOVE)) {
            tankAction.setLength(new Random().nextInt(tank.getYidong() + 1));
        } else {
            tankAction.setLength(new Random().nextInt(tank.getShecheng() + 1));
        }
        return null;
    }


    public Direct toDirection(Tank tank, View view) {
        List<String> unMove = Arrays.asList("M4", "M5", "M6", "M7", "M8", "B1", "B2", "B3", "B4", "B5");
        unMove.remove(tank.gettId());
        List<List<String>> luZhangLocation = getLocationsByTypes(view.getMap(), unMove);
        List<String> luZhangList = luZhangLocation.stream().map(list -> String.join("," + list)).collect(Collectors.toList());
        List<List<String>> tankLocation = getLocationsByTypes(view.getMap(), Collections.singletonList(tank.gettId()));
        for (List<String> location : tankLocation) {
            Integer tankX = Integer.parseInt(location.get(0));
            Integer tankY = Integer.parseInt(location.get(1));
            String toDown = String.join(String.valueOf(tankX + 1), ",", String.valueOf(tankY));
            String toUp = String.join(String.valueOf(tankX - 1), ",", String.valueOf(tankY));
            String toLeft = String.join(String.valueOf(tankX), ",", String.valueOf(tankY - 1));
            String toRight = String.join(String.valueOf(tankX), ",", String.valueOf(tankY + 1));
            Direct direct;
            Direct behindDirect = behindDirection.get(tank.gettId());
            if (new HashSet<>(luZhangList).containsAll(Arrays.asList(toDown, toUp, toLeft, toRight))) {
                direct = Direct.WAIT;
            } else if (!luZhangList.contains(toDown) && luZhangList.contains(toUp) && luZhangList.contains(toLeft) && luZhangList.contains(toRight)) {
                direct = tankX == view.getRowLen() - 1 ? Direct.WAIT : Direct.DOWN;
            } else if (luZhangList.contains(toDown) && !luZhangList.contains(toUp) && luZhangList.contains(toLeft) && luZhangList.contains(toRight)) {
                direct = tankX == 0 ? Direct.WAIT : Direct.UP;
            } else if (luZhangList.contains(toDown) && luZhangList.contains(toUp) && !luZhangList.contains(toLeft) && luZhangList.contains(toRight)) {
                direct = tankY == 0 ? Direct.WAIT : Direct.LEFT;
            } else if (luZhangList.contains(toDown) && luZhangList.contains(toUp) && luZhangList.contains(toLeft) && !luZhangList.contains(toRight)) {
                direct = tankY == view.getColLen() - 1 ? Direct.WAIT : Direct.RIGHT;
            } else if (!luZhangList.contains(toDown) && !luZhangList.contains(toUp) && luZhangList.contains(toLeft) && luZhangList.contains(toRight)) {
                if (tankX == 0) {
                    direct = Direct.DOWN;
                } else if (tankX == view.getRowLen() - 1) {
                    direct = Direct.UP;
                } else {
                    direct = behindDirect.equals(Direct.DOWN) || behindDirect.equals(Direct.UP) ? behindDirect : Direct.DOWN;
                }
            } else if (luZhangList.contains(toDown) && luZhangList.contains(toUp) && !luZhangList.contains(toLeft) && !luZhangList.contains(toRight)) {
                if (tankY == 0) {
                    direct = Direct.RIGHT;
                } else if (tankY == view.getColLen() - 1) {
                    direct = Direct.LEFT;
                } else {
                    direct = behindDirect.equals(Direct.RIGHT) || behindDirect.equals(Direct.LEFT) ? behindDirect : Direct.RIGHT;
                }
            } else if (luZhangList.contains(toDown) && !luZhangList.contains(toUp) && !luZhangList.contains(toLeft) && luZhangList.contains(toRight)) {
                if (tankX == 0 && tankY == 0) {
                    direct = Direct.WAIT;
                } else if (tankX != 0 && tankY != 0) {
                    direct = behindDirect.equals(Direct.UP) || behindDirect.equals(Direct.LEFT) ? behindDirect : Direct.LEFT;
                } else if (tankX != 0) {
                    direct = Direct.UP;
                } else {
                    direct = Direct.LEFT;
                }

            } else if (!luZhangList.contains(toDown) && luZhangList.contains(toUp) && luZhangList.contains(toLeft) && !luZhangList.contains(toRight)) {
                if (tankX == 0 && tankY == 0) {
                    direct = Direct.WAIT;
                } else if (tankX != 0 && tankY != 0) {
                    direct = behindDirect.equals(Direct.DOWN) || behindDirect.equals(Direct.RIGHT) ? behindDirect : Direct.RIGHT;
                } else if (tankX != 0) {
                    direct = Direct.DOWN;
                } else {
                    direct = Direct.RIGHT;
                }
            } else if (!luZhangList.contains(toDown) && luZhangList.contains(toUp) && !luZhangList.contains(toLeft) && luZhangList.contains(toRight)) {
                if (tankX == 0 && tankY == 0) {
                    direct = Direct.WAIT;
                } else if (tankX != 0 && tankY != 0) {
                    direct = behindDirect.equals(Direct.DOWN) || behindDirect.equals(Direct.LEFT) ? behindDirect : Direct.LEFT;
                } else if (tankX != 0) {
                    direct = Direct.DOWN;
                } else {
                    direct = Direct.LEFT;
                }
            } else if (luZhangList.contains(toDown) && !luZhangList.contains(toUp) && !luZhangList.contains(toLeft) && !luZhangList.contains(toRight)) {
                if (tankX == 0 && tankY == 0) {
                    direct = Direct.WAIT;
                } else if (tankX != 0 && tankY != 0) {
                    direct = behindDirect.equals(Direct.UP) || behindDirect.equals(Direct.RIGHT) ? behindDirect : Direct.RIGHT;
                } else if (tankX != 0) {
                    direct = Direct.UP;
                } else {
                    direct = Direct.RIGHT;
                }
            } else if (!luZhangList.contains(toDown) && !luZhangList.contains(toUp) && !luZhangList.contains(toLeft) && luZhangList.contains(toRight)) {
                if (tankX == 0 && tankY == 0) {
                    direct = Direct.DOWN;
                } else if (tankX == view.getRowLen() - 1 && tankY == 0) {
                    direct = Direct.UP;
                }
            }
        }
        return null;
    }

    /**
     * 获取地图中元素的坐标
     *
     * @param map       地图
     * @param statement 元素
     * @return List<List < Integer>>    坐标
     */
    public List<List<String>> getLocationsByTypes(List<List<String>> map, List<String> statement) {
        List<List<String>> result = new ArrayList<>();
        for (int i = 0; i < map.size(); i++) {
            List<String> list = map.get(i);
            for (int j = 0; j < list.size(); j++) {
                String x = list.get(j);
                for (String type : statement) {
                    if (x.equals(type)) {
                        List<String> li = new ArrayList<>();
                        li.add(String.valueOf(i));
                        li.add(String.valueOf(j));
                        result.add(li);
                    }
                }
            }
        }
        return result;
    }


    public static void main(String[] args) {
        PlayerService playerService = new PlayerService();
        String jsonStr = "{\n" + "\t\t\"team\": \"tB\",\n" + "\t\t\"view\": {\n" + "\t\t\t\"name\": \"25X20\",\n" + "\t\t\t\"rowLen\": 20,\n" + "\t\t\t\"colLen\": 25,\n" + "\t\t\t\"map\": [\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M4\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M4\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M4\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M4\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M1\",\"M1\",\"M1\",\"M1\",\"M1\",\"M1\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M1\",\"M1\",\"M1\",\"M1\",\"M1\",\"M1\",\"M1\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"],\n" + "\t\t\t\t[\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M1\",\"C1\",\"C2\",\"C3\",\"C4\",\"C5\",\"M1\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\",\"M3\"]\n" + "\t\t\t]\n" + "\t\t},\n" + "\t\t\"tA\": {\n" + "\t\t\t\"glod\": 0,\n" + "\t\t\t\"name\": \"BOSS\",\n" + "\t\t\t\"tanks\": [\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"A1\",\n" + "\t\t\t\t\t\"name\": \"BOSS\",\n" + "\t\t\t\t\t\"gongji\": 0,\n" + "\t\t\t\t\t\"shengming\": 15,\n" + "\t\t\t\t\t\"shengyushengming\": 15,\n" + "\t\t\t\t\t\"yidong\": 1,\n" + "\t\t\t\t\t\"shecheng\": 1,\n" + "\t\t\t\t\t\"shiye\": 1\n" + "\t\t\t\t}\n" + "\t\t\t],\n" + "\t\t\t\"extend\": 0\n" + "\t\t},\n" + "\t\t\"tB\": {\n" + "\t\t\t\"glod\": 0,\n" + "\t\t\t\"name\": \"小马流星队\",\n" + "\t\t\t\"tanks\": [\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"B1\",\n" + "\t\t\t\t\t\"name\": \"K2黑豹\",\n" + "\t\t\t\t\t\"gongji\": 3,\n" + "\t\t\t\t\t\"shengming\": 5,\n" + "\t\t\t\t\t\"shengyushengming\": 5,\n" + "\t\t\t\t\t\"yidong\": 1,\n" + "\t\t\t\t\t\"shecheng\": 1,\n" + "\t\t\t\t\t\"shiye\": 1\n" + "\t\t\t\t},\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"B2\",\n" + "\t\t\t\t\t\"name\": \"T-90\",\n" + "\t\t\t\t\t\"gongji\": 1,\n" + "\t\t\t\t\t\"shengming\": 10,\n" + "\t\t\t\t\t\"shengyushengming\": 10,\n" + "\t\t\t\t\t\"yidong\": 1,\n" + "\t\t\t\t\t\"shecheng\": 1,\n" + "\t\t\t\t\t\"shiye\": 1\n" + "\t\t\t\t},\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"B3\",\n" + "\t\t\t\t\t\"name\": \"阿马塔\",\n" + "\t\t\t\t\t\"gongji\": 1,\n" + "\t\t\t\t\t\"shengming\": 4,\n" + "\t\t\t\t\t\"shengyushengming\": 4,\n" + "\t\t\t\t\t\"yidong\": 2,\n" + "\t\t\t\t\t\"shecheng\": 1,\n" + "\t\t\t\t\t\"shiye\": 2\n" + "\t\t\t\t},\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"B4\",\n" + "\t\t\t\t\t\"name\": \"阿马塔\",\n" + "\t\t\t\t\t\"gongji\": 1,\n" + "\t\t\t\t\t\"shengming\": 4,\n" + "\t\t\t\t\t\"shengyushengming\": 4,\n" + "\t\t\t\t\t\"yidong\": 2,\n" + "\t\t\t\t\t\"shecheng\": 1,\n" + "\t\t\t\t\t\"shiye\": 2\n" + "\t\t\t\t},\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"B5\",\n" + "\t\t\t\t\t\"name\": \"99主战坦克\",\n" + "\t\t\t\t\t\"gongji\": 1,\n" + "\t\t\t\t\t\"shengming\": 3,\n" + "\t\t\t\t\t\"shengyushengming\": 3,\n" + "\t\t\t\t\t\"yidong\": 1,\n" + "\t\t\t\t\t\"shecheng\": 3,\n" + "\t\t\t\t\t\"shiye\": 1\n" + "\t\t\t\t}\n" + "\t\t\t],\n" + "\t\t\t\"extend\": 0\n" + "\t\t},\n" + "\t\t\"tC\": {\n" + "\t\t\t\"glod\": 0,\n" + "\t\t\t\"name\": \"我的队\",\n" + "\t\t\t\"tanks\": [\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"C1\",\n" + "\t\t\t\t\t\"name\": \"K2黑豹\",\n" + "\t\t\t\t\t\"gongji\": 3,\n" + "\t\t\t\t\t\"shengming\": 5,\n" + "\t\t\t\t\t\"shengyushengming\": 5,\n" + "\t\t\t\t\t\"yidong\": 1,\n" + "\t\t\t\t\t\"shecheng\": 1,\n" + "\t\t\t\t\t\"shiye\": 1\n" + "\t\t\t\t},\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"C2\",\n" + "\t\t\t\t\t\"name\": \"T-90\",\n" + "\t\t\t\t\t\"gongji\": 1,\n" + "\t\t\t\t\t\"shengming\": 10,\n" + "\t\t\t\t\t\"shengyushengming\": 10,\n" + "\t\t\t\t\t\"yidong\": 1,\n" + "\t\t\t\t\t\"shecheng\": 1,\n" + "\t\t\t\t\t\"shiye\": 1\n" + "\t\t\t\t},\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"C3\",\n" + "\t\t\t\t\t\"name\": \"阿马塔\",\n" + "\t\t\t\t\t\"gongji\": 1,\n" + "\t\t\t\t\t\"shengming\": 4,\n" + "\t\t\t\t\t\"shengyushengming\": 4,\n" + "\t\t\t\t\t\"yidong\": 2,\n" + "\t\t\t\t\t\"shecheng\": 1,\n" + "\t\t\t\t\t\"shiye\": 2\n" + "\t\t\t\t},\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"C4\",\n" + "\t\t\t\t\t\"name\": \"阿马塔\",\n" + "\t\t\t\t\t\"gongji\": 1,\n" + "\t\t\t\t\t\"shengming\": 4,\n" + "\t\t\t\t\t\"shengyushengming\": 4,\n" + "\t\t\t\t\t\"yidong\": 2,\n" + "\t\t\t\t\t\"shecheng\": 1,\n" + "\t\t\t\t\t\"shiye\": 2\n" + "\t\t\t\t},\n" + "\t\t\t\t{\n" + "\t\t\t\t\t\"tId\": \"C5\",\n" + "\t\t\t\t\t\"name\": \"99主战坦克\",\n" + "\t\t\t\t\t\"gongji\": 1,\n" + "\t\t\t\t\t\"shengming\": 3,\n" + "\t\t\t\t\t\"shengyushengming\": 3,\n" + "\t\t\t\t\t\"yidong\": 1,\n" + "\t\t\t\t\t\"shecheng\": 3,\n" + "\t\t\t\t\t\"shiye\": 1\n" + "\t\t\t\t}\n" + "\t\t\t],\n" + "\t\t\t\"extend\": 0\n" + "\t\t}\n" + "\t}";
        GameMap gameMap = JSON.parseObject(jsonStr).to(GameMap.class);
        System.out.println(playerService.getLocationsByTypes(gameMap.getView().getMap(), Arrays.asList("M4")));
        List<String> list = new ArrayList<>();
        list.add("ads");
        list.add("saf");
        System.out.println(String.join("fasfas", "2322"));
        System.out.println(Direct.DOWN.equals(null));
    }

}

