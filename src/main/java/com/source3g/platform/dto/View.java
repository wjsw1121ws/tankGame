package com.source3g.platform.dto;

import lombok.Data;

import java.util.List;

/**
 * 你所在队伍的视野
 */
public class View {

    private String name;
    private int rowLen;
    private int colLen;
    private List<List<String>> map;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRowLen() {
        return rowLen;
    }

    public void setRowLen(int rowLen) {
        this.rowLen = rowLen;
    }

    public int getColLen() {
        return colLen;
    }

    public void setColLen(int colLen) {
        this.colLen = colLen;
    }

    public List<List<String>> getMap() {
        return map;
    }

    public void setMap(List<List<String>> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "View{" +
                "name='" + name + '\'' +
                ", rowLen=" + rowLen +
                ", colLen=" + colLen +
                ", map=" + map +
                '}';
    }
}
