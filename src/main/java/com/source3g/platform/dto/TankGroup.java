package com.source3g.platform.dto;

import java.util.List;

/**
 * 战队明细
 */
public class TankGroup {

    private String name; // 战队名称
    private List<Tank> tanks; // 坦克详情

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tank> getTanks() {
        return tanks;
    }

    public void setTanks(List<Tank> tanks) {
        this.tanks = tanks;
    }

    @Override
    public String toString() {
        return "TankGroup{" +
                "name='" + name + '\'' +
                ", tanks=" + tanks +
                '}';
    }
}
