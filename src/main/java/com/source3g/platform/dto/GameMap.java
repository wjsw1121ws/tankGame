package com.source3g.platform.dto;

/**
 * 地图信息
 */
public class GameMap {

    private int gold; // 复活币数量,可用来复活某个坦克
    private String team; // 系统指定你所在的队伍，有(B/C),你只对对该队伍的坦克有操作权
    private View view; // 你所在队伍的视野
    private TankGroup tA; // BOSS明细
    private TankGroup tB; // B队明细
    private TankGroup tC; // C队明细

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public TankGroup gettA() {
        return tA;
    }

    public void settA(TankGroup tA) {
        this.tA = tA;
    }

    public TankGroup gettB() {
        return tB;
    }

    public void settB(TankGroup tB) {
        this.tB = tB;
    }

    public TankGroup gettC() {
        return tC;
    }

    public void settC(TankGroup tC) {
        this.tC = tC;
    }

    @Override
    public String toString() {
        return "GameMap{" +
                "gold=" + gold +
                ", team='" + team + '\'' +
                ", view=" + view +
                ", tA=" + tA +
                ", tB=" + tB +
                ", tC=" + tC +
                '}';
    }
}
