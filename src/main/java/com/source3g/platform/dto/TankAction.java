package com.source3g.platform.dto;

import com.source3g.platform.contants.Direct;
import com.source3g.platform.contants.Type;
import lombok.Data;

/**
 * 坦克开打接口返回
 */
public class TankAction {

    private String tId; //坦克ID，分别是A1,B(1-5),C(1-5)
    private Direct direction; // 移动或攻击方向,有UP、RIGHT、DOWN、LEFT、WAIT
    private Type type; // action类型，有MOVE,FIRE
    private int length; // 移动或攻击距离
    private boolean useGlod; // 是否使用复活币

    public TankAction() {
    }

    public TankAction(String tId, Direct direction) {
        this.tId = tId;
        this.direction = direction;
    }

    public String gettId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }

    public Direct getDirection() {
        return direction;
    }

    public void setDirection(Direct direction) {
        this.direction = direction;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isUseGlod() {
        return useGlod;
    }

    public void setUseGlod(boolean useGlod) {
        this.useGlod = useGlod;
    }

    @Override
    public String toString() {
        return "TankAction{" +
                "tId='" + tId + '\'' +
                ", direction=" + direction +
                ", type=" + type +
                ", length=" + length +
                ", useGlod=" + useGlod +
                '}';
    }
}
