package com.source3g.platform.dto;

import java.io.Serializable;

public class ApiResult<T> implements Serializable {

    private String code;
    private String action;
    private String msg;
    private T data;
    private Boolean ok = false;

    public static <T> ApiResult<T> fail(String action, String msg) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.code = "-1";
        apiResult.action = action;
        apiResult.msg = msg;
        apiResult.ok = false;
        return apiResult;
    }

    public static <T> ApiResult<T> success(String action, T data) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.code = "0";
        apiResult.action = action;
        apiResult.msg = "succeeded";
        apiResult.ok = true;
        apiResult.data = data;
        return apiResult;
    }

    public static <T> ApiResult<T> success(String action, String msg) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.code = "0";
        apiResult.action = action;
        apiResult.msg = msg;
        apiResult.ok = true;
        return apiResult;
    }

    public ApiResult() {
    }

    public ApiResult(String code, String action, String msg, T data) {
        this.code = code;
        this.action = action;
        this.msg = msg;
        this.data = data;
        if ("-1".equals(code)) {
            this.ok = false;
        } else {
            this.ok = true;
        }

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    @Override
    public String toString() {
        return "ApiResult{" +
                "code='" + code + '\'' +
                ", action='" + action + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", ok=" + ok +
                '}';
    }
}
