package com.source3g.platform.dto;

/**
 * 坦克属性
 */
public class Tank {

    private String tId; // 坦克ID，分别是A1,B(1-5),C(1-5)
    private String name;
    private int gongji;
    private int shengming;
    private int shengyushengming;
    private int yidong;
    private int shecheng;
    private int shiye;

    public String gettId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGongji() {
        return gongji;
    }

    public void setGongji(int gongji) {
        this.gongji = gongji;
    }

    public int getShengming() {
        return shengming;
    }

    public void setShengming(int shengming) {
        this.shengming = shengming;
    }

    public int getShengyushengming() {
        return shengyushengming;
    }

    public void setShengyushengming(int shengyushengming) {
        this.shengyushengming = shengyushengming;
    }

    public int getYidong() {
        return yidong;
    }

    public void setYidong(int yidong) {
        this.yidong = yidong;
    }

    public int getShecheng() {
        return shecheng;
    }

    public void setShecheng(int shecheng) {
        this.shecheng = shecheng;
    }

    public int getShiye() {
        return shiye;
    }

    public void setShiye(int shiye) {
        this.shiye = shiye;
    }

    @Override
    public String toString() {
        return "Tank{" +
                "tId='" + tId + '\'' +
                ", name='" + name + '\'' +
                ", gongji=" + gongji +
                ", shengming=" + shengming +
                ", shengyushengming=" + shengyushengming +
                ", yidong=" + yidong +
                ", shecheng=" + shecheng +
                ", shiye=" + shiye +
                '}';
    }
}
