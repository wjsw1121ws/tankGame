package com.source3g.platform.dto;

import com.source3g.platform.service.PlayerCommon;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: W10005176
 * @version: 1.0
 * @blame: Test Team
 **/
public class TankPosition implements Serializable {
    private static final long serialVersionUID = -7704136598375655486L;

    private Tank tank;
    private List<Integer> xy;
    private Boolean isTeamB;
    private Boolean isTeamC;
    private String left;
    private String right;
    private String up;
    private String down;
    private String leftInAttract;
    private String rightInAttract;
    private String upInAttract;
    private String downInAttract;
    private String leftInMove;
    private String rightInMove;
    private String upInMove;
    private String downInMove;

    public TankPosition(View view, Tank tank) {
        this.tank = tank;
        String tankId = this.tank.gettId();
        int attract = tank.getShecheng();
        int move = tank.getYidong();
        List<List<String>> map = view.getMap();
        Map<String, List<List<Integer>>> tankLocation = PlayerCommon.getLocationsByTankId(map, tankId);
        if (!CollectionUtils.isEmpty(tankLocation)) {
            this.xy = tankLocation.get(tankId).get(0);
        }
        this.isTeamB = tankId.startsWith("B");
        this.isTeamC = tankId.startsWith("C");
        if (isTeamB) {
            if (!CollectionUtils.isEmpty(xy) && xy.get(1) - 1 > 0) {
                this.left = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) - 1));
            }
            if (!CollectionUtils.isEmpty(xy) && xy.get(1) + 1 <= (view.getColLen()) * 0.65) {
                this.right = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) + 1));
            }
        } else {
            if (!CollectionUtils.isEmpty(xy) && xy.get(1) - 1 >= (view.getColLen()) * 0.35) {
                this.left = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) - 1));
            }
            if (!CollectionUtils.isEmpty(xy) && xy.get(1) + 1 < (view.getColLen())) {
                this.right = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) + 1));
            }
        }
        if (!CollectionUtils.isEmpty(xy) && xy.get(0) - 1 >= 0) {
            this.up = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0) - 1, xy.get(1)));
        }
        if (!CollectionUtils.isEmpty(xy) && xy.get(0) + 1 < view.getRowLen()) {
            this.down = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0) + 1, xy.get(1)));
        }
        /*if (!CollectionUtils.isEmpty(xy) && xy.get(1) - 1 > 0) {
            this.left = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) - 1));
        }
        if (!CollectionUtils.isEmpty(xy) && xy.get(1) + 1 < (view.getColLen())) {
            this.right = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) + 1));
        }*/

        if (attract == 1) {
            this.upInAttract = this.up;
            this.downInAttract = this.down;
            this.leftInAttract = this.left;
            this.rightInAttract = this.right;
        } else {
            if (!CollectionUtils.isEmpty(xy) && xy.get(0) - attract > 0) {
                this.upInAttract = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0) - attract, xy.get(1)));
            }
            if (!CollectionUtils.isEmpty(xy) && xy.get(0) + attract < view.getRowLen()) {
                this.downInAttract = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0) + attract, xy.get(1)));
            }
            if (!CollectionUtils.isEmpty(xy) && xy.get(1) - attract > 0) {
                this.leftInAttract = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) - attract));
            }
            if (!CollectionUtils.isEmpty(xy) && xy.get(1) + attract < (view.getColLen())) {
                this.rightInAttract = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) + attract));
            }
        }

        if (move == 1) {
            this.upInMove = this.up;
            this.downInMove = this.down;
            this.leftInMove = this.left;
            this.rightInMove = this.right;
        } else {
            if (isTeamB) {
                if (!CollectionUtils.isEmpty(xy) && xy.get(1) - move > 0) {
                    this.leftInMove = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) - move));
                }
                if (!CollectionUtils.isEmpty(xy) && xy.get(1) + move <= (view.getColLen()) * 0.65) {
                    this.rightInMove = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) + move));
                }
            } else {
                if (!CollectionUtils.isEmpty(xy) && xy.get(1) - move >= (view.getColLen() - 1) * 0.35) {
                    this.leftInMove = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) - move));
                }
                if (!CollectionUtils.isEmpty(xy) && xy.get(1) + move < (view.getColLen() - 1)) {
                    this.rightInMove = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) + move));
                }
            }
            if (!CollectionUtils.isEmpty(xy) && xy.get(0) - move > 0) {
                this.upInMove = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0) - move, xy.get(1)));
            }
            if (!CollectionUtils.isEmpty(xy) && xy.get(0) + move < view.getRowLen()) {
                this.downInMove = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0) + move, xy.get(1)));
            }
            /*if (!CollectionUtils.isEmpty(xy) && xy.get(1) - move > 0) {
                this.leftInMove = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) - move));
            }
            if (!CollectionUtils.isEmpty(xy) && xy.get(1) + move <= (view.getColLen())) {
                this.rightInMove = PlayerCommon.getTypeByLocation(map, Arrays.asList(xy.get(0), xy.get(1) + move));
            }*/
        }
    }

    public List<Integer> getXy() {
        return xy;
    }

    public void setXy(List<Integer> xy) {
        this.xy = xy;
    }

    public Boolean getIsTeamB() {
        return isTeamB;
    }

    public void setIsTeamB(Boolean teamB) {
        isTeamB = teamB;
    }

    public Boolean getIsTeamC() {
        return isTeamC;
    }

    public void setIsTeamC(Boolean teamC) {
        isTeamC = teamC;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getUp() {
        return up;
    }

    public void setUp(String up) {
        this.up = up;
    }

    public String getDown() {
        return down;
    }

    public void setDown(String down) {
        this.down = down;
    }

    public String getLeftInAttract() {
        return leftInAttract;
    }

    public void setLeftInAttract(String leftInAttract) {
        this.leftInAttract = leftInAttract;
    }

    public String getRightInAttract() {
        return rightInAttract;
    }

    public void setRightInAttract(String rightInAttract) {
        this.rightInAttract = rightInAttract;
    }

    public String getUpInAttract() {
        return upInAttract;
    }

    public void setUpInAttract(String upInAttract) {
        this.upInAttract = upInAttract;
    }

    public String getDownInAttract() {
        return downInAttract;
    }

    public void setDownInAttract(String downInAttract) {
        this.downInAttract = downInAttract;
    }

    public String getLeftInMove() {
        return leftInMove;
    }

    public void setLeftInMove(String leftInMove) {
        this.leftInMove = leftInMove;
    }

    public String getRightInMove() {
        return rightInMove;
    }

    public void setRightInMove(String rightInMove) {
        this.rightInMove = rightInMove;
    }

    public String getUpInMove() {
        return upInMove;
    }

    public void setUpInMove(String upInMove) {
        this.upInMove = upInMove;
    }

    public String getDownInMove() {
        return downInMove;
    }

    public void setDownInMove(String downInMove) {
        this.downInMove = downInMove;
    }

    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }
}
