package com.source3g.platform.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * @author: W10005176
 * @version: 1.0
 * @blame: Test Team
 **/
public class Location implements Serializable {
    private static final long serialVersionUID = 7950888963926364422L;
    private String name;
    private List<List<Integer>> locationList;
    private String type;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<List<Integer>> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<List<Integer>> locationList) {
        this.locationList = locationList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
